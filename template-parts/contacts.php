<?php
/*
Template Name: Контакты
*/
?>
<?php get_header(); ?>
    <!--main-content-->
    <main class="main main--margin">
    <div class="container">
    <!--breadcrumbs-->
    <div class="row">
        <div class="col-12">
            <?php
            if (function_exists('yoast_breadcrumb')) :
                yoast_breadcrumb('<p class="breadcrumbs" id="breadcrumbs">', '</p>');
            endif;
            ?>
        </div>
    </div>
    <!--heading-->
    <div class="row">
    <div class="col-12">
        <h1 class="title-h1"><?php the_title(); ?></h1>
    </div>
    </div>
    <!--contacts-->
     <div class="row">
            <div class="col-12 col-lg-6 order-2 order-lg-1">
                <div class="map-block">
                    <h3 class="title-h3 text-start mb-3">Местонахождение на карте</h3>
                    <!--contacts-map-->
                    <div style="position: relative; overflow: hidden;"><iframe style="position: relative;" src="https://yandex.kz/map-widget/v1/-/CCUiqBuLHB" width="560" height="400" frameborder="1" allowfullscreen="allowfullscreen"></iframe></div>
                </div>
            </div>
            <div class="col-12 col-lg-6 order-1 order-lg-2">

                <!--contacts-block-->
                <div class="contacts-block">
                    <h3 class="title-h3 text-start mb-3">Наш адрес</h3>
                    <p class="contacts-block__subtitle">Клиника Региональный Акушерско-Гинекологических Центр
                        находится по адресу:</p>
                    <p class="contacts-block__text">Город Караганда, Октябрьский район, Майкудук, улица Щорса 53</p>

                </div>
                <!--contacts-block-->
                <div class="contacts-block">
                    <h3 class="title-h3 text-start mb-3">Наши контакты</h3>
                    <div class="contacts-grid">
                        <div class="contacts-container">
                            <div class="contacts-wrapper">
                                <p class="contacts-block__subtitle">Регистратура:</p>

                                <div class="d-flex flex-column"><a class="contacts-wrapper__link" href="tel:+77078837530">Cотовый : + 7 (707) 88 375 30</a></div>
                            </div>
                            <div class="contacts-wrapper">
                                <p class="contacts-block__subtitle">Акушерский стационар:</p>
                                <a class="contacts-wrapper__link" href="tel:+77078837533">Cотовый : +7 (707) 88 375 33</a>

                            </div>
                            <div class="contacts-wrapper">
                                <p class="contacts-block__subtitle">Гинекологический стационар:</p>
                                <a class="contacts-wrapper__link" href="tel:+77078837531">Cотовый : +7 (707) 88 375 31</a>

                            </div>
                            <div class="contacts-wrapper">
                                <p class="contacts-block__subtitle">Служба поддержки пациентов:</p>

                                <div class="d-flex flex-column"><a class="contacts-wrapper__link" href="tel:+77212460297">Стационарный : + 7 (7212) 46 02 97</a></div>
                            </div>
                        </div>
                        <div class="contacts-container">
                            <div class="contacts-wrapper">
                                <p class="contacts-block__subtitle">Заместитель Директора:</p>
                                <a class="contacts-wrapper__link" href="tel:+77212460294">Стационарный : + 7 (7212) 46 02 94</a>

                            </div>
                            <div class="contacts-wrapper">
                                <p class="contacts-block__subtitle">Бухгалтерия:</p>
                                <a class="contacts-wrapper__link" href="tel:+77212460191">Стационарный : + 7 (7212) 46 01 91</a>

                            </div>
                            <div class="contacts-wrapper">
                                <p class="contacts-block__subtitle">Приемная:</p>
                                <a class="contacts-wrapper__link" href="tel:+77212460191">Стационарный : + 7 (7212) 46 01 91</a>

                            </div>
                            <div class="contacts-wrapper">
                                <p class="contacts-block__subtitle">Почтовый адресс:</p>
                                <a class="contacts-wrapper__link" href="mailto:ragc@mail.ru">ragc@mail.ru</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>