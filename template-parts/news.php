<?php
/*
Template Name: Новости
*/
?>
<?php get_header(); ?>
    <!--main-content-->
    <main class="main main--margin">
        <div class="container">
            <!--breadcrumbs-->
            <div class="row">
                <div class="col-12">
                    <?php
                    if (function_exists('yoast_breadcrumb')) :
                        yoast_breadcrumb('<p class="breadcrumbs" id="breadcrumbs">', '</p>');
                    endif;
                    ?>
                </div>
            </div>
            <!--heading-->
            <div class="row">
                <div class="col-12">
                    <h1 class="title-h1"><?php the_title(); ?></h1>
                </div>
            </div>
            <!--news-page-content-->
            <div class="row specialists">
                        <?php $args = array(
                            'post_type' => 'post',
                            'category_name'=> 'news',
                            'posts_per_page' => -1
                        ); ?>
                        <?php $news = new WP_Query($args);
                        // дальше - loop
                        if ($news->have_posts()) :
                            while ($news->have_posts()) :
                                $news->the_post(); ?>
                                <!--news-block-->
                                <div class="col-12 col-md-6 col-lg-12 d-flex align-items-stretch">
                                    <section class="news-block">
                                        <?php the_post_thumbnail( 380,240 );?>
                                        <article class="news-wrapper">
                                            <div class="d-flex flex-column">
                                            <h3 class="title-h3 news-wrapper__heading"><?php the_title(); ?></h3>
                                            <address class="news-wrapper__author"><a href="<?php the_field('author'); ?>" rel="author" class="news-wrapper__link"><?php the_field('author'); ?></a></address>
                                                <?php echo kama_excerpt( [ 'maxchar'=>200,  ] ); ?>
                                            </div>
                                            <div class="news-buttons">
                                                <p class="news-buttons__date"><?php the_time( 'j F Y');?></p>
                                                <div class="d-flex align-items-center">
                                                    <a href="<?php the_permalink();?>" class="header-slide__link">
                                                        <span class="header-slide__span">Узнать подробнее</span>
                                                        <svg class="header-slide__icon">
                                                            <use xlink:href="<?php echo get_template_directory_uri() ?>/assets/img/sprite.svg#slider-arrow"></use>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </article>
                                    </section>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>