<?php
/*
Template Name: Прейскурант цен
*/
?>
<?php get_header(); ?>
<!--main-content-->
<main class="main main--margin ">
    <!--prices-content-->
    <div class="container">
        <!--breadcrumbs-->
        <div class="row">
            <div class="col-12">
                <?php
                if ( function_exists( 'yoast_breadcrumb' ) ) :
                    yoast_breadcrumb( '<p class="breadcrumbs" id="breadcrumbs">', '</p>' );
                endif;
                ?>
            </div>
        </div>
        <!--prices-heading-->
        <div class="row">
            <div class="col-12 text-start">
                <h1 class="title-h1 mb-3">Прайс-лист</h1>
                <p class="prices-block__subtitle">Прейскурант цен на платные услуги, оказываемые в ТОО «РАГЦ» с
                    01
                    октября 2021 года</p>
            </div>
        </div>
        <!--prices-buttons-->
        <!--TODO-->
        <!--<div class="row">
            <div class="col-12">
                <div class="prices-buttons">
                    <a href="#" class="prices-buttons__link">Консультация и диагностика</a>
                    <a href="#" class="prices-buttons__link">УЗИ</a>
                    <a href="#" class="prices-buttons__link">Прерывание беременности</a>
                    <a href="#" class="prices-buttons__link">Пребывание в палате</a>
                    <a href="#" class="prices-buttons__link">Контракт c врачом</a>
                    <a href="#" class="prices-buttons__link">Лапаротомные и лапароскопические операции</a>
                    <a href="#" class="prices-buttons__link">Лабораторные исследования</a>
                    <a href="#" class="prices-buttons__link">Физиотерапевтические услуги</a>
                </div>
            </div>
        </div>-->
        <!--prices-block-->
        <div class="row">
            <div class="col-12 mb-40">
                <div class="prices-block">
                    <!--prices-search-->
                    <form action="#" class="prices-form">
                        <div class="prices-form__icon">
                            <input type="text" class="prices-form__search" id="pricesSearch" placeholder="Начните вводить название услуги">
                        </div>
                    </form>
                    <!--prices-info-->
                    <div class="prices-table prices-table--heading">
                        <p class="prices-table__heading">Наименование услуги</p>
                        <p class="prices-table__heading text-center">Стоимость (тенге)</p>
                        <p class="prices-table__heading text-center">Примечание</p>
                    </div>
                    <!--prices-wrapper-->
                    <div class="prices-wrapper prices-wrapper--bg ">
                        <!--prices-category-->
                        <div class="prices-category">
                            <!--prices--heading-->
                            <p class="prices-category__heading">Консультация и диагностика:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием доцента кафедры КГМА, зам.
                                    директора по
                                    лечебной части</p>
                                <p class="prices-table__price">8 000 тг.</p>
                                <p class="prices-table__note">Один прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача акушера-гинеколога
                                </p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Один прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прием психолога</p>
                                <p class="prices-table__price">5 000 тг.</p>
                                <p class="prices-table__note">Один прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача мамолога</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Первый прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача мамолога (повторный)
                                </p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Повторный прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача онколога</p>
                                <p class="prices-table__price">5 000 тг.</p>
                                <p class="prices-table__note">Один прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача терапевта</p>
                                <p class="prices-table__price">5 000 тг.</p>
                                <p class="prices-table__note">Первый прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультация врача репродуктолога с УЗИ
                                    исследованием</p>
                                <p class="prices-table__price">8 000 тг.</p>
                                <p class="prices-table__note">Первый прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультация врача репродуктолога с УЗИ
                                    исследованием (повторный)</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Повторный прием</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">УЗИ диагностика:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ гинекологическое</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ фолликулометрия</p>
                                <p class="prices-table__price">2 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ лимфатических узлов </p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ мочевого пузыря</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ почек</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ плода</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ лонного сочленения</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ с доплерометрией </p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ плода с доплерометрией </p>
                                <p class="prices-table__price">4 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ гастро-дуоденальной зоны</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ мочеполовой системы комплексно </p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ молочных желез</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ щитовидной железы</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ щитовидной железы</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ предстательной железы</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ гастро-дуоденальной зоны с почками </p>
                                <p class="prices-table__price">5 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ коленных суставов </p>
                                <p class="prices-table__price">3 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ мошонки</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ мягких тканей и лимфоузлы</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ультразвуковая диагностика магистральных сосудов
                                    (БЦС)</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ультразвуковая диагностика магистральных сосудов
                                    (БЦС)</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗДГ БЦС+ транскраниальное исследование (сосудов
                                    шеи и головы)</p>
                                <p class="prices-table__price">9 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">ЭХО-КС (УЗИ сердца) </p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сосудов нижней конечности - вены
                                </p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сосудов нижней конечности - артерии
                                </p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сосудов нижней конечности - вены +
                                    артерии </p>
                                <p class="prices-table__price">11 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сосудов верхней конечности </p>
                                <p class="prices-table__price">7 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗДГ сосудов почек</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">НСГ</p>
                                <p class="prices-table__price">3 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Кольпоскопия</p>
                                <p class="prices-table__price">10 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Прерывание береммености:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прерывание беременности по сроку до 20-ти дней
                                    (миниаборт)</p>
                                <p class="prices-table__price">18 000 тг.</p>
                                <p class="prices-table__note">Одна проц.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прерывание беременности по сроку до 10 недель</p>
                                <p class="prices-table__price">20 000 тг.</p>
                                <p class="prices-table__note">Одна проц.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прерывание беременности в сроке от 10 до 12
                                    недель (с патологией матки)</p>
                                <p class="prices-table__price">21 000 тг.</p>
                                <p class="prices-table__note">Одна проц.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Диагностическое выскабливание полости матки в
                                    стационарных условиях</p>
                                <p class="prices-table__price">24 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    до 7-ми недель</p>
                                <p class="prices-table__price">20 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 8-10 недель</p>
                                <p class="prices-table__price">30 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 8-10 недель</p>
                                <p class="prices-table__price">30 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 10-12 недель</p>
                                <p class="prices-table__price">30 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 13 до 15-ти недель</p>
                                <p class="prices-table__price">40 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 13 до 15-ти недель</p>
                                <p class="prices-table__price">40 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 16 до 21 недель</p>
                                <p class="prices-table__price">60 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прерывание беременности в сроке свыше 16 до 21
                                    недели путем малого «Кесарево сечения» по социальным показаниям</p>
                                <p class="prices-table__price">100 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Пребывание в палате:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Регионарная антибиотикотерапия, в сочетании с
                                    массажем и физиолечением, в условиях дневного стационара</p>
                                <p class="prices-table__price">20 000 тг.</p>
                                <p class="prices-table__note">Дневной стационар</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Лечение воспалительных процессов женской половой
                                    сферы в стационарных условиях</p>
                                <p class="prices-table__price">34 000 тг.</p>
                                <p class="prices-table__note">Стационар</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Содержание в палате с комфортными условиями - 1
                                    категории</p>
                                <p class="prices-table__price">15 000 тг.</p>
                                <p class="prices-table__note">Сутки</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Содержание в палате с комфортными условиями - 2
                                    категории</p>
                                <p class="prices-table__price">10 500 тг.</p>
                                <p class="prices-table__note">Сутки</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Дородовое наблюдение беременных</p>
                                <p class="prices-table__price">120 000 тг.</p>
                                <p class="prices-table__note">Амбулаторно</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в «Люкс-палате» после физиологических
                                    родов</p>
                                <p class="prices-table__price">158 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в «Люкс-палате» после операции
                                    «Кесарево сечение»</p>
                                <p class="prices-table__price">208 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 2-х местной палате после
                                    физиологических родов</p>
                                <p class="prices-table__price">50 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 2-х местной палате после операции
                                    «Кесарево сечение»</p>
                                <p class="prices-table__price">75 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 1 местной палате после
                                    физиологических родов</p>
                                <p class="prices-table__price">45 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 1 местной палате после операции
                                    «Кесарево сечение»</p>
                                <p class="prices-table__price">55 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 1 местной Люкс палате после
                                    физиологических родов</p>
                                <p class="prices-table__price">90 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 1 местной Люкс палате после операции
                                    «Кесарево сечение»</p>
                                <p class="prices-table__price">120 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 2-х местной Люкс палате после
                                    физиологических родов (отдел.физ.№2)</p>
                                <p class="prices-table__price">75 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title"> Пребывание в 2-х местной Люкс палате после
                                    операции «Кесарево сечение» (отдел.физ.№2)</p>
                                <p class="prices-table__price">100 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Контракт с врачом:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на роды с врачом</p>
                                <p class="prices-table__price">140 000 тг.</p>
                                <p class="prices-table__note">Одна услуга</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на роды с зам. Директора, с
                                    зав.отделением</p>
                                <p class="prices-table__price">200 000 тг.</p>
                                <p class="prices-table__note">Одна услуга</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на роды с акушеркой</p>
                                <p class="prices-table__price">85 000 тг.</p>
                                <p class="prices-table__note">Одна услуга</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на кесарево сечение</p>
                                <p class="prices-table__price">160 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на кесарево сечение с зам. Директора, с
                                    зав.отделением</p>
                                <p class="prices-table__price">240 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Лапаротомные и лапароскопические операции:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пластические операции-два этапа</p>
                                <p class="prices-table__price">350 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Миома матки, бесплодие</p>
                                <p class="prices-table__price">230 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Операция на придатках</p>
                                <p class="prices-table__price">230 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Диагностическая гистероскопия </p>
                                <p class="prices-table__price">55 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Гистероскопическая полипэктомия</p>
                                <p class="prices-table__price">69 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Гистероскопическая миомэктомия </p>
                                <p class="prices-table__price">80 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Добровольная хирургическая стерилизация</p>
                                <p class="prices-table__price">150 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Экстирпация матки</p>
                                <p class="prices-table__price">240 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Вскрытие абсцесса бартолиниевой железы</p>
                                <p class="prices-table__price">40 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Киста бартолиниевой железы</p>
                                <p class="prices-table__price">75 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пластика восстановления промежности</p>
                                <p class="prices-table__price">180 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Плазмолифтинг в гинекологии</p>
                                <p class="prices-table__price">17 500 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Эпидуральная анестезия</p>
                                <p class="prices-table__price">30 000 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Плазмоферез</p>
                                <p class="prices-table__price">30 200 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Операция подкожная мостэктомия (гинекомастия) с
                                    одной стороны</p>
                                <p class="prices-table__price">138 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Операция секторальная резекция молочной железы
                                </p>
                                <p class="prices-table__price">90 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Операция удаление добавочной молочной железы с
                                    одной стороны</p>
                                <p class="prices-table__price">100 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Иссечение доброкачественных образований</p>
                                <p class="prices-table__price">40 000 тг.</p>
                                <p class="prices-table__note">Малая операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Трепан-биопсия</p>
                                <p class="prices-table__price">23 500 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Трепан-биопсия с УЗИ навигацией</p>
                                <p class="prices-table__price">27 500тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Тонкоигольная аспирационная биопсия (пункционная
                                    биопсия)</p>
                                <p class="prices-table__price">10 000 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Внутривенный наркоз с пропофолом (наблюдение до
                                    полного пробуждения)</p>
                                <p class="prices-table__price">12 000 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Перенатальный скрининг на преэклампсию (по
                                    Тарификатору)</p>
                                <p class="prices-table__price">11 805 тг.</p>
                                <p class="prices-table__note">Одна услуга</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Введение ВМС</p>
                                <p class="prices-table__price">12 000 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Извлечение ВМС</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Электрохирургическое лечение шейки матки</p>
                                <p class="prices-table__price">15 500 тг.</p>
                                <p class="prices-table__note">Один курс</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Лабораторные исследования:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Общий биохимический анализ крови на аппарате
                                    «Анализатор гематологический»</p>
                                <p class="prices-table__price">3 435 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение общего белка</p>
                                <p class="prices-table__price">450 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение мочевины в сыворотке крови</p>
                                <p class="prices-table__price">475 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение билирубина</p>
                                <p class="prices-table__price">535 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование крови на сахар</p>
                                <p class="prices-table__price">590 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение АЛАТ, АСАТ</p>
                                <p class="prices-table__price">445 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение креатинина</p>
                                <p class="prices-table__price">495 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Общий анализ крови на аппарате «Анализатор
                                    гематологический» </p>
                                <p class="prices-table__price">1 935 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Взятие крови</p>
                                <p class="prices-table__price">405 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение гемоглобина</p>
                                <p class="prices-table__price">355 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение эритроцитов</p>
                                <p class="prices-table__price">365 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение лейкоцитов</p>
                                <p class="prices-table__price">410 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение СОЭ </p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Развернутый анализ крови на аппарате
                                    «Анализатор гематологический»</p>
                                <p class="prices-table__price">3 290 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Взятие крови (развернутый анализ)</p>
                                <p class="prices-table__price">325 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение гемоглобина (развернутый анализ)</p>
                                <p class="prices-table__price">355 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение эритроцитов (развернутый анализ)</p>
                                <p class="prices-table__price">365 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение лейкоцитов (развернутый анализ)</p>
                                <p class="prices-table__price">410 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение СОЭ (развернутый анализ)</p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение тромбоцитов</p>
                                <p class="prices-table__price">430 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение гемотокрита</p>
                                <p class="prices-table__price">495 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование общего анализа мочи</p>
                                <p class="prices-table__price">740 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Анализ мочи по Нечипоренко</p>
                                <p class="prices-table__price">560 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Анализ мочи по Зимницкому</p>
                                <p class="prices-table__price">430 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Анализ на желчные пигменты</p>
                                <p class="prices-table__price">470 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование мазков на гонококкии и трихомоны</p>
                                <p class="prices-table__price">690 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сахара крови глюкометром</p>
                                <p class="prices-table__price">520 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение времени свертываемости крови</p>
                                <p class="prices-table__price">410 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Кал на скрытую кровь</p>
                                <p class="prices-table__price">370 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Кал на копроскопию </p>
                                <p class="prices-table__price">700 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Молозивные тельца</p>
                                <p class="prices-table__price">365 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование околоплодных вод</p>
                                <p class="prices-table__price">370 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование мазков на антипичные клетки</p>
                                <p class="prices-table__price">490 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Коагулограмма (ПВ, ПО, ПТИ, АЧТВ, МНО, фибриноген)</p>
                                <p class="prices-table__price">1 010 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение группы крови и резус-фактора</p>
                                <p class="prices-table__price">640 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Кольпоцитология </p>
                                <p class="prices-table__price">460 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение антител и титра</p>
                                <p class="prices-table__price">525 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование семенной жидкости </p>
                                <p class="prices-table__price">855 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Сахар мочи - тест полосками </p>
                                <p class="prices-table__price">370 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Микрореакция</p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">ВИЧ</p>
                                <p class="prices-table__price">3 800 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">ВИЧ</p>
                                <p class="prices-table__price">3 800 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Спермограмма</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">RW</p>
                                <p class="prices-table__price">3 000т тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Гепатит</p>
                                <p class="prices-table__price">4 300 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Общий бета ХГЧ на ап-те Анализатор</p>
                                <p class="prices-table__price">4 300 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Д-Димер на ап-те Анализатор </p>
                                <p class="prices-table__price">4 600 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прокальцитон на ап-те Анализатор</p>
                                <p class="prices-table__price">7 300 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">С-реактивный белок на ап-те Анализатор</p>
                                <p class="prices-table__price">3 700 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ферритин на ап-те Анализатор</p>
                                <p class="prices-table__price">4 600 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Физиотерапевтические услуги:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Электрофорез</p>
                                <p class="prices-table__price">530 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ректальный электрофорез</p>
                                <p class="prices-table__price">500 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УВЧ-терапия</p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Токи Бернара с введением лекарственных средств</p>
                                <p class="prices-table__price">425 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Дарсонвализация местная</p>
                                <p class="prices-table__price">440 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Микроволновая терапия (аппарат (ЛУЧ-2)</p>
                                <p class="prices-table__price">420 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УФО-местное</p>
                                <p class="prices-table__price">405 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УФО-тубус</p>
                                <p class="prices-table__price">500 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ультразвук</p>
                                <p class="prices-table__price">410 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Солюкс</p>
                                <p class="prices-table__price">480 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Полостной электронагреватель (ПЭН)</p>
                                <p class="prices-table__price">420 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Внутривенная инъекция</p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Внутримышечная инъекция</p>
                                <p class="prices-table__price">355 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Система</p>
                                <p class="prices-table__price">595 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Выдача архивных справок по запросам физических лиц</p>
                                <p class="prices-table__price">1 000 тг.</p>
                                <p class="prices-table__note">Одна справка</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
