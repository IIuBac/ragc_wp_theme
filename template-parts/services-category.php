<?php
/*
Template Name: Перечень услуг
*/
?>
<?php get_header(); ?>
    <!--main-content-->
    <main class="main main--margin">
        <div class="container">
            <!--breadcrumbs-->
            <div class="row">
                <div class="col-12">
                    <?php
                    if (function_exists('yoast_breadcrumb')) :
                        yoast_breadcrumb('<p class="breadcrumbs" id="breadcrumbs">', '</p>');
                    endif;
                    ?>
                </div>
            </div>
            <!--heading-->
            <div class="row">
                <div class="col-12">
                    <h1 class="title-h1"><?php the_title(); ?></h1>
                </div>
            </div>
            <!--services-page-content-->
            <div class="row pages-styles">
                <!--services-category-block-->
                <?php $args = array(
                    'post_type' => 'services-category',
                ); ?>
                <?php $services = new WP_Query($args);
                if ($services->have_posts()) :
                    while ($services->have_posts()) :
                        $services->the_post(); ?>
                        <!--category-block-->
                        <div class="col-12 col-md-6 col-lg-4 d-flex align-items-stretch">
                            <div class="services-category">
                                <?php the_post_thumbnail(); ?>
                                <div class="services-category__container">
                                    <h2 class="services-category__heading"><?php the_title(); ?></h2>
                                    <p class="services-category__text"><?php echo kama_excerpt(['maxchar' => 100,]); ?></p>
                                    <a href="<?php the_permalink(); ?>" class="category-link">
                                        <span class="category-link__text">Подробнее</span>
                                        <svg class="category-icon">
                                            <path d="M6.95508 4.5L11.4551 9L6.95508 13.5" class="category-icon__path"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>