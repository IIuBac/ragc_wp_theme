<?php
/*
Template Name: Гос.закупки
*/
?>
<?php get_header(); ?>
    <!--main-content-->
    <main class="main main--margin">
        <div class="container">
            <!--breadcrumbs-->
            <div class="row">
                <div class="col-12">
                    <?php
                    if ( function_exists( 'yoast_breadcrumb' ) ) :
                        yoast_breadcrumb( '<p class="breadcrumbs" id="breadcrumbs">', '</p>' );
                    endif;
                    ?>
                </div>
            </div>
            <!--heading-->
            <div class="row">
                <div class="col-12">
                    <h1 class="title-h1"><?php the_title(); ?></h1>
                </div>
            </div>
            <!--gov-procurement-->
            <div class="row">
                <!--gov-procurement-block-->
                <?php $args = array(
                    'post_type' => 'gov-procurement',
                    'posts_per_page' => -1,
                ); ?>
                <?php $procurement = new WP_Query($args);
                // дальше - loop
                if ($procurement->have_posts()) :
                    while ($procurement->have_posts()) :
                        $procurement->the_post(); ?>
                        <div class="col-12">
                            <div class="gov-block">
                                <a href="<?php the_field('add_link'); ?>" class="gov-link" download="">
                                    <svg class="gov-link__icon">
                                        <use xlink:href="<?php echo get_template_directory_uri() ?>/assets/img/sprite.svg#pdf-icon"></use>
                                    </svg>
                                    <?php the_title('<p class="gov-link__text">', '</p>'); ?>
                                </a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>

            </div>
        </div>
    </main>
<?php get_footer(); ?>