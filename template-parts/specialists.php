<?php
/*
Template Name: Список специалистов
*/
?>
<?php get_header(); ?>
    <!--main-content-->
    <main class="main main--margin">
        <div class="container">
            <!--breadcrumbs-->
            <div class="row">
                <div class="col-12">
                    <?php
                    if (function_exists('yoast_breadcrumb')) :
                        yoast_breadcrumb('<p class="breadcrumbs" id="breadcrumbs">', '</p>');
                    endif;
                    ?>
                </div>
            </div>
            <!--heading-->
            <div class="row">
                <div class="col-12">
                    <h1 class="title-h1"><?php the_title(); ?></h1>
                </div>
            </div>
            <!--services-page-content-->
            <div class="row specialists">
                <div class="col-12">
                    <div class="specialists-wrapper">
                        <!--specialists-wrapper-->
                        <?php $args = array(
                            'post_type' => 'specialists',
                            'posts_per_page' => -1,
                        ); ?>
                        <?php $specialists = new WP_Query($args);
                        // дальше - loop
                        if ($specialists->have_posts()) :
                            while ($specialists->have_posts()) :
                                $specialists->the_post(); ?>
                                <!--specialists-block-->
                                <div class="specialists-block">
                                    <?php the_post_thumbnail(); ?>
                                    <p class="specialists-block__name"><?php the_title(); ?></p>
                                    <p class="specialists-block__position"><?php the_field('position'); ?></p>
                                    <a href="<?php the_permalink(); ?>" class="standard-link"><?php echo esc_attr( pll__( 'Подробнее' ) )  ?></a>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>