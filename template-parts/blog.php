<?php
/*
Template Name: Блог директора
*/
?>
<?php get_header(); ?>
<!--main-content-->
<main class="main main--margin">
    <div class="container">
        <!--breadcrumbs-->
        <div class="row">
            <div class="col-12">
                <?php
                if ( function_exists( 'yoast_breadcrumb' ) ) :
                    yoast_breadcrumb( '<p class="breadcrumbs" id="breadcrumbs">', '</p>' );
                endif;
                ?>
            </div>
        </div>
        <!--heading-->
        <div class="row">
            <div class="col-12">
                <h1 class="title-h1"><?php the_title(); ?></h1>
            </div>
        </div>
        <!--blog-container-->
        <div class="row">
            <!--blog-form-->
            <div class="col--12 col-sm-12 col-md-12 col-lg-6">
                <p class="form-heading">Заполните форму ниже и опишите интересующий вас вопрос:</p>
                <form action="" class="blog-form">
                    <!--form-container-->
                    <div class="form-container">
                        <!--blog-text-->
                        <input type="text" class="blog-form__text" placeholder="Имя">
                        <!--blog-text-->
                        <input type="text" class="blog-form__text" placeholder="Фамилия">
                        <!--blog-text-->
                        <input type="text" class="blog-form__text" placeholder="Номер телефона">
                        <!--blog-text-->
                        <input type="text" class="blog-form__text" placeholder="Электронный адрес">
                        <!--blog-textarea-->
                        <textarea name="" id="" cols="30" rows="10" class="blog-form__textarea" placeholder="Текст обращения"></textarea>
                        <!--blog-textarea-->
                        <input type="submit" class="standard-link standard-link__input">
                    </div>
                </form>
            </div>
            <!--blog-img-->
            <div class="col--12 col-sm-12 col-md-12 col-lg-6">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/director-blog-img.svg" alt="director-blog" class="blog-img">
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>