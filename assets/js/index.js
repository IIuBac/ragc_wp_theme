//header-slider//
new Swiper(".header-slider", {
    pagination: {
        el: ".swiper-pagination",
        clickable: true,

    },
    autoplay: {
        delay: 5000,
    },
});
//reviews-slider//
var swiper = new Swiper(".reviews-slider", {
    slidesPerView: 1,
    spaceBetween: 5,
    pagination: {
        el: ".swiper-pagination",
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});
//footer-slider//
new Swiper ('.footer-slider',{
    slidesPerView: 5,
    spaceBetween: 30,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
   pagination: {
       el: ".swiper-pagination",
       clickable: true,
   },
    breakpoints: {
        320: {
            slidesPerView: 1,
        },
        480: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 30
        },
        1440: {
            slidesPerView: 5,
            spaceBetween: 30
        }
    }
});
//prices-seacrh//
window.onload = () => {
    const pricesSearch = document.querySelector('#pricesSearch')
    const categories = document.querySelectorAll('.prices-category')
    const allTitles = document.querySelectorAll('.prices-wrapper .prices-table .prices-table__title')

    pricesSearch.oninput = function () {
        const value = this.value.trim().toLowerCase()

        if (value) {
            categories.forEach(category => {
                let hiddenTitlesCount = 0
                const titles = category.querySelectorAll('.prices-table .prices-table__title')

                titles.forEach(title => {
                    const wrapper = title.closest('.prices-table')
                    if (!title.innerText.toLowerCase().includes(value)){
                        wrapper.classList.add('hide')
                        hiddenTitlesCount++
                    } else {
                        wrapper.classList.remove('hide')
                    }
                })

                if (hiddenTitlesCount === titles.length) {
                    category.classList.add('hide')
                } else {
                    category.classList.remove('hide')
                }
            })

            return
        }

        categories.forEach(category => {
            category.classList.remove('hide')
        })

        allTitles.forEach(elem =>{
            elem.closest('.prices-table').classList.remove('hide')
        })
    }
}
//navbar-menu//

//button-more//
$(function() {
    $(".characteristic-block__text--hidden").slice(0, 4).show();
    $("#loadMore").on('click', function(e) {
        e.preventDefault();
        $(".characteristic-block__text--hidden:hidden").slice(0, 4).slideDown();
        if ($(".characteristic-block__text--hidden:hidden").length == 0) {
            $("#loadLess").fadeIn('slow');
            $("#loadMore").hide();
            // $("#loadMore").text('Load only the first 4');
        }
    });

    $("#loadLess").on('click', function(e) {
        e.preventDefault();
        $('.characteristic-block__text--hidden:not(:lt(4))').fadeOut();
        $("#loadMore").fadeIn('slow');
        $("#loadLess").hide();
    });

});
//button-move//
function buttonMove(x) {
    x.classList.toggle("move");
}
//mobile-menu//
let mobileButton = document.querySelector('.mobile-burger');
let mobileMenu = document.querySelector('.mobile-navbar');
    mobileButton.addEventListener('click', _=>{
        mobileMenu.classList.toggle('mobile-navbar--active');
    });

let mobileExit = document.querySelector('.mobile-navbar__exit');
    mobileExit.addEventListener('click', _=>{
        mobileMenu.classList.remove('mobile-navbar--active');
    });
//prices-button//
let pricesContainer = document.querySelector('.prices-wrapper-roll');
let pricesButton = document.querySelector('.prices-button');
        pricesButton.addEventListener('click', _=>{
            pricesContainer.classList.toggle('open-wrapper');
});
const pricesButtonText = document.querySelector('.prices-button');
pricesButtonText.addEventListener('click', function() {
    pricesButtonText.innerHTML =
        (pricesButtonText.innerHTML === 'Открыть прайс-лист') ? pricesButtonText.innerHTML = 'Закрыть прайс-лист' : pricesButtonText.innerHTML = 'Открыть прайс-лист';
})