<?php get_header(); ?>
<!--main-content-->
<main class="main main--margin">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!--breadcrumbs-->
                <div class="row">
                    <div class="col-12">
                        <?php
                        if ( function_exists( 'yoast_breadcrumb' ) ) :
                            yoast_breadcrumb( '<p class="breadcrumbs" id="breadcrumbs">', '</p>' );
                        endif;
                        ?>
                    </div>
                </div>
                <!--heading-->
                <div class="row">
                    <div class="col-12">
                        <h1 class="title-h1"><?php the_title(); ?></h1>
                    </div>
                </div>
                <!--specialists-page-content-->
                <div class="row specialists-styles">
                    <div class="col-12 ">
                    <!--specialist-card-->
                    <?php get_header();?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <!--specialists-card-->
                            <div class="specialists-card">
                                <?php the_post_thumbnail(); ?>
                                <div class="characteristic-wrapper">
                                    <!--specialists-experience-->
                                    <div class="characteristic-block">
                                        <h3 class="characteristic-block__title"><?php echo esc_attr( pll__( 'Места и стаж работы:' ) )  ?></h3>
                                        <p class="characteristic-block__text"><?php the_field('experience'); ?></p>
                                    </div>
                                    <!--specialists-studies-->
                                    <div class="characteristic-block">
                                        <h3 class="characteristic-block__title"><?php echo esc_attr( pll__( 'Образование:' ) )  ?></h3>
                                       <?php the_field('study'); ?>
                                    </div>
                                    <!--specialists-studies-->
                                    <div class="characteristic-block">
                                        <h3 class="characteristic-block__title"><?php echo esc_attr( pll__( 'Специальность:' ) )  ?></h3>
                                        <p class="characteristic-block__text"><?php the_field('special'); ?></p>
                                    </div>
                                    <!--specialists-position-->
                                    <div class="characteristic-block">
                                        <h3 class="characteristic-block__title"><?php echo esc_attr( pll__( 'Должность:' ) )  ?></h3>
                                        <p class="characteristic-block__text"><?php the_field('position'); ?></p>
                                    </div>
                                    <!--specialists-qualification-->
                                    <div class="characteristic-block">
                                        <h3 class="characteristic-block__title"><?php echo esc_attr( pll__( 'Квалификационная категория:' ) )  ?></h3>
                                        <?php the_field('training'); ?>
                                    </div>
                                    <!--specialists-courses-->
                                    <div class="characteristic-block">
                                        <h3 class="characteristic-block__title"><?php echo esc_attr( pll__( 'Курсы повышения квалификации:' ) )  ?></h3>
                                        <?php the_field('qualification'); ?>
                                    </div>
                                    <!--specialists-certificates-->
                                    <div class="characteristic-block">
                                        <h3 class="characteristic-block__title"><?php echo esc_attr( pll__( 'Награды:' ) )  ?></h3>
                                        <ul class="certificate-block">
                                            <?php the_field('certificates'); ?>
                                        </ul>
                                        <!--TODO-->
                                        <!--<a href="#" id="loadMore" class="load-more-btn">Показать больше</a>
                                        <a href="#" id="loadLess" class="load-more-btn">Показать меньше</a>-->
                                    </div>
                                </div>
                            </div>
                    <?php endwhile; else : ?>
                        <p>Записей нет.</p>
                    <?php endif; ?>
                </div>
                <div class="col-12 text-center mt-40">
                    <!--back-button-->
                    <a href="/about-us/specialists/" class="standard-link"><?php echo esc_attr( pll__( 'Назад' ) )  ?></a>
                </div>
            </div>
        </div>
    </div>

</main>
<?php get_footer(); ?>

