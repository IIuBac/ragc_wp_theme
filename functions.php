<?php
/*Add-thumbnails*/
add_theme_support( 'post-thumbnails', array( 'post','services-category','specialists') );
/*Template-styles*/
add_action('wp_enqueue_scripts', 'add_files');

function add_files() {
    wp_enqueue_style('bootstrap-css', get_template_directory_uri(). '/assets/css/bootstrap.min.css');
    wp_enqueue_style('style-css', get_template_directory_uri(). '/style.css');
    wp_enqueue_style('swiper-css', get_template_directory_uri(). '/assets/css/swiper.min.css');
    wp_enqueue_script('popper-js', get_template_directory_uri(). '/assets/js/popper.min.js', array('jquery'), null, 'footer');
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri(). '/assets/js/jquery.min.js', false, null, true );
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap-js', get_template_directory_uri(). '/assets/js/bootstrap.min.js', array('jquery'), null, 'footer' );
    wp_enqueue_script('swiper-bundle-js',get_template_directory_uri(). '/assets/js/swiper-bundle.min.js', array('jquery'), null, 'footer');
    wp_enqueue_script('index-js', get_template_directory_uri(). '/assets/js/index.js', false, null, 'footer');
    wp_enqueue_style('roboto-fonts', '//fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700');
    wp_enqueue_style('montserrat-fonts', '//fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,700;0,800;1,400;1,500');
}
/*Navbar-walker*/
function register_navwalker(){
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

/*Register-primary-menu*/
add_action( 'after_setup_theme', 'theme_register_primary_menu' );

function theme_register_primary_menu() {
    register_nav_menu( 'primary', 'Главное меню' );
}

/*Register-mobile-menu*/
add_action( 'after_setup_theme', 'theme_register_mobile_menu' );

function theme_register_mobile_menu() {
    register_nav_menu( 'mobile', 'Мобильное меню' );
}
/*SVG-loading*/
add_filter( 'upload_mimes', 'svg_upload_allow' );

# Добавляет SVG в список разрешенных для загрузки файлов.
function svg_upload_allow( $mimes ) {
    $mimes['svg']  = 'image/svg+xml';

    return $mimes;
}
/*Gov-procurement-custom-posts*/
add_action( 'init', 'register_post_procurement' );
function register_post_procurement(){
    register_post_type( 'gov-procurement', [
        'label'  => null,
        'labels' => [
            'name'               => 'Гос.закупки', // основное название для типа записи
            'singular_name'      => 'Гос.закупка', // название для одной записи этого типа
            'add_new'            => 'Добавить Гос.закупку', // для добавления новой записи
            'add_new_item'       => 'Добавление Гос.закупки', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование Гос.закупки', // для редактирования типа записи
            'new_item'           => 'Новая Гос.закупка', // текст новой записи
            'view_item'          => 'Смотреть Гос.закупку', // для просмотра записи этого типа.
            'search_items'       => 'Искать Гос.закупка', // для поиска по этим типам записи
            'not_found'          => 'Не найдено Гос.закупок', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено Гос.закупок в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Гос.закупки', // название меню
        ],
        'description'         => '',
        'public'              => true,
        'show_in_menu'        => null, // показывать ли в меню адмнки
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        'hierarchical'        => false,
        'supports'            => [ 'title', 'editor', 'custom-fields' ],
        'taxonomies'          => [],
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ] );
}
/*Gov-orders-custom-posts*/
add_action( 'init', 'register_post_orders' );
function register_post_orders(){
    register_post_type( 'gov-orders', [
        'label'  => null,
        'labels' => [
            'name'               => 'Приказы', // основное название для типа записи
            'singular_name'      => 'Приказ', // название для одной записи этого типа
            'add_new'            => 'Добавить Приказ', // для добавления новой записи
            'add_new_item'       => 'Добавление Приказа', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование Приказа', // для редактирования типа записи
            'new_item'           => 'Новый Приказ', // текст новой записи
            'view_item'          => 'Смотреть Приказ', // для просмотра записи этого типа.
            'search_items'       => 'Искать Приказы', // для поиска по этим типам записи
            'not_found'          => 'Не найдено Приказов', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено Приказов в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Приказы', // название меню
        ],
        'description'         => '',
        'public'              => true,
        'show_in_menu'        => null, // показывать ли в меню адмнки
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        'hierarchical'        => false,
        'supports'            => [ 'title', 'custom-fields' ],
        'taxonomies'          => [],
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ] );
}
/*Gov-procurement-custom-posts*/
add_action( 'init', 'register_post_services_category' );
function register_post_services_category(){
    register_post_type( 'services-category', [
        'label'  => null,
        'labels' => [
            'name'               => 'Услуги', // основное название для типа записи
            'singular_name'      => 'Услуга', // название для одной записи этого типа
            'add_new'            => 'Добавить услугу', // для добавления новой записи
            'add_new_item'       => 'Добавление услуги', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование услуги', // для редактирования типа записи
            'new_item'           => 'Новая услуга', // текст новой записи
            'view_item'          => 'Смотреть услугу', // для просмотра записи этого типа.
            'search_items'       => 'Искать услугу', // для поиска по этим типам записи
            'not_found'          => 'Не найдено услуг', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено услуг в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Перечень услуг', // название меню
        ],
        'description'         => '',
        'public'              => true,
        'show_in_menu'        => null, // показывать ли в меню адмнки
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        'hierarchical'        => false,
        'supports'            => [ 'title', 'editor', 'custom-fields', 'excerpt', 'thumbnail' ],
        'taxonomies'          => [],
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ] );
}
/*Gov-procurement-custom-posts*/
add_action( 'init', 'register_post_specialists' );
function register_post_specialists(){
    register_post_type( 'specialists', [
        'label'  => null,
        'labels' => [
            'name'               => 'Специалисты', // основное название для типа записи
            'singular_name'      => 'Специалист', // название для одной записи этого типа
            'add_new'            => 'Добавить специалиста', // для добавления новой записи
            'add_new_item'       => 'Добавление специалиста', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование специалиста', // для редактирования типа записи
            'new_item'           => 'Новый специалист', // текст новой записи
            'view_item'          => 'Смотреть специалиста', // для просмотра записи этого типа.
            'search_items'       => 'Искать специалиста', // для поиска по этим типам записи
            'not_found'          => 'Не найдено специалистов', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено специалистов в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Специалисты', // название меню
        ],
        'description'         => '',
        'public'              => true,
        'show_in_menu'        => null, // показывать ли в меню адмнки
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        'hierarchical'        => false,
        'supports'            => [ 'title', 'custom-fields', 'thumbnail' ],
        'taxonomies'          => [],
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ] );
}
/*Gov-procurement-custom-posts*/
add_action( 'init', 'register_post_memo' );
function register_post_memo(){
    register_post_type( 'memo-docs', [
        'label'  => null,
        'labels' => [
            'name'               => 'Памятки', // основное название для типа записи
            'singular_name'      => 'Памятка', // название для одной записи этого типа
            'add_new'            => 'Добавить памятка', // для добавления новой записи
            'add_new_item'       => 'Добавление памятки', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование памятки', // для редактирования типа записи
            'new_item'           => 'Новая памятка', // текст новой записи
            'view_item'          => 'Смотреть памятку', // для просмотра записи этого типа.
            'search_items'       => 'Искать памятку', // для поиска по этим типам записи
            'not_found'          => 'Не найдено памяток', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено памяток в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Памятки', // название меню
        ],
        'description'         => '',
        'public'              => true,
        'show_in_menu'        => null, // показывать ли в меню адмнки
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        'hierarchical'        => false,
        'supports'            => [ 'title', 'custom-fields'],
        'taxonomies'          => [],
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ] );
}
/*Kama-excerpt*/
function kama_excerpt( $args = '' ){
    global $post;

    if( is_string( $args ) ){
        parse_str( $args, $args );
    }

    $rg = (object) array_merge( [
        'maxchar'           => 350,
        'text'              => '',
        'autop'             => true,
        'more_text'         => 'Читать дальше...',
        'ignore_more'       => false,
        'save_tags'         => '<strong><b><a><em><i><var><code><span>',
        'sanitize_callback' => static function( string $text, object $rg ){
            return strip_tags( $text, $rg->save_tags );
        },
    ], $args );

    $rg = apply_filters( 'kama_excerpt_args', $rg );

    if( ! $rg->text ){
        $rg->text = $post->post_excerpt ?: $post->post_content;
    }

    $text = $rg->text;
    // strip content shortcodes: [foo]some data[/foo]. Consider markdown
    $text = preg_replace( '~\[([a-z0-9_-]+)[^\]]*\](?!\().*?\[/\1\]~is', '', $text );
    // strip others shortcodes: [singlepic id=3]. Consider markdown
    $text = preg_replace( '~\[/?[^\]]*\](?!\()~', '', $text );
    // strip direct URLs
    $text = preg_replace( '~(?<=\s)https?://.+\s~', '', $text );
    $text = trim( $text );

    // <!--more-->
    if( ! $rg->ignore_more && strpos( $text, '<!--more-->' ) ){

        preg_match( '/(.*)<!--more-->/s', $text, $mm );

        $text = trim( $mm[1] );

        $text_append = sprintf( ' <a href="%s#more-%d">%s</a>', get_permalink( $post ), $post->ID, $rg->more_text );
    }
    // text, excerpt, content
    else {

        $text = call_user_func( $rg->sanitize_callback, $text, $rg );
        $has_tags = false !== strpos( $text, '<' );

        // collect html tags
        if( $has_tags ){
            $tags_collection = [];
            $nn = 0;

            $text = preg_replace_callback( '/<[^>]+>/', static function( $match ) use ( & $tags_collection, & $nn ){
                $nn++;
                $holder = "~$nn";
                $tags_collection[ $holder ] = $match[0];

                return $holder;
            }, $text );
        }

        // cut text
        $cuted_text = mb_substr( $text, 0, $rg->maxchar );
        if( $text !== $cuted_text ){

            // del last word, it not complate in 99%
            $text = preg_replace( '/(.*)\s\S*$/s', '\\1...', trim( $cuted_text ) );
        }

        // bring html tags back
        if( $has_tags ){
            $text = strtr( $text, $tags_collection );
            $text = force_balance_tags( $text );
        }
    }

    // add <p> tags. Simple analog of wpautop()
    if( $rg->autop ){

        $text = preg_replace(
            [ "/\r/", "/\n{2,}/", "/\n/" ],
            [ '', '</p><p>', '<br />' ],
            "<p>$text</p>"
        );
    }

    $text = apply_filters( 'kama_excerpt', $text, $rg );

    if( isset( $text_append ) ){
        $text .= $text_append;
    }

    return $text;
}
get_template_part( 'translate' );

