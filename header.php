<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo wp_get_document_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body>
<!--wrapper-->
<div class="wrapper">
    <!--header-->
    <header class="header">
        <!--header-info-->
        <div class="d-none d-xl-block">
            <div class="info-block">
                <div class="container">
                    <!--header-links-->
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex align-items-center justify-content-between">
                                <a href="tel:+77078837530" class="info-block__link"><span
                                            class="info-block__text"><?php echo esc_attr(pll__('Регистратура:')) ?></span>
                                    +7 707 88 375 30</a>
                                <a href="tel:+77078837533" class="info-block__link"><span
                                            class="info-block__text"><?php echo esc_attr(pll__('Акушерский стационар:')) ?></span>
                                    +7 707 88 375 33</a>
                                <a href="tel:+77078837530" class="info-block__link"><span
                                            class="info-block__text"><?php echo esc_attr(pll__('Гинекологический стационар:')) ?></span>
                                    +7 707 88 375 31</a>
                                <a href="mailto:ragc@mail.ru" class="info-block__link"><span
                                            class="info-block__ text"><?php echo esc_attr(pll__('Почта:')) ?></span>
                                    ragc@mail.ru</a>
                                <a href="https://www.instagram.com/ragc_krg2017/?hl=ru"
                                   class="info-block__link info-block__link--footer" target="_blank"><span
                                            class="info-block__ text">Instagram:</span> ragc_krg2017</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header-menu-->
        <div class="container d-none d-xl-block">
            <div class="header-menu">
                <!--header-logo-->
                <a href="/" class="header-logo">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/logo.svg" alt="logo"
                         class="header-logo__img">
                    <p class="header-logo__text"><?php echo esc_attr(pll__('Региональный Акушерско Гинекологический Центр')) ?></p>
                </a>
                <!--header-nav-->
                <div class="d-flex">
                    <!--navbar-wrapper-->
                    <?php
                    wp_nav_menu(array(
                        'menu' => 'primary',
                        'container' => 'div',
                        'container_class' => 'd-flex',
                        'menu_class' => 'navbar-menu',
                        'depth' => 3,
                        'walker' => new wp_bootstrap_navwalker()
                    ));
                    ?>
                </div>
                <!--header-lang-->
                <ul class="header-lang"><?php pll_the_languages(); ?></ul>
            </div>
        </div>
        <hr class="header-hr">
        <!--header-mobile-menu-->
        <div class="mobile-menu d-flex d-xl-none">
            <a href="/" class="mobile-menu__link">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/logo.svg" alt="logo"
                     class="mobile-menu__logo">
                <p class="mobile-menu__text"><?php echo esc_attr(pll__('Региональный Акушерско Гинекологический Центр')) ?></p>
            </a>
            <!--mobile--burder-->
            <div class="mobile-burger" onclick="buttonMove(this)">
                <span class="mobile-burger__span mobile-burger__span--bar-1"></span>
                <span class="mobile-burger__span mobile-burger__span--bar-2"></span>
                <span class="mobile-burger__span mobile-burger__span--bar-3"></span>
            </div>
            <!---mobile-navbar-->
            <div class="mobile-navbar">
                <div class="d-flex align-items-center justify-content-between mb-4">
                    <p class="mobile-navbar__heading"><?php echo esc_attr(pll__('Меню')) ?></p>
                    <button class="mobile-navbar__exit">&#215;</button>
                </div>
                <!---mobile-list-->
                <?php
                wp_nav_menu(array(
                    'menu' => 'mobile',
                    'container' => 'ul',
                    'container_class' => 'mobile-list',
                    'depth' => 3,
                    'walker' => new wp_bootstrap_navwalker()
                ));
                ?>
                <!---mobile-hr-->
                <hr class="mobile-hr">
                <!---mobile-lang-->
                <ul class="mobile-lang"><?php pll_the_languages(); ?></ul>
                <!---mobile-hr-->
                <hr class="mobile-hr">
            </div>
        </div>
    </header>