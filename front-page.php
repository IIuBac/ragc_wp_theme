<?php
/*
Template Name: Default template
*/
?>
<?php get_header(); ?>
<!--main-content-->
<main class="main">
    <!--header-slider-->
    <div class="container">
        <!-- header-slider-container-->
        <div class="swiper header-slider">
            <div class="swiper-wrapper header-slider__wrapper">
                <!-- header-slide-->
                <div class="swiper-slide header-slide">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider-img/slider-img-home.JPG" alt="slider-img-home" class="header-slide__img">
                    <div class="container slide-content">
                        <p class="header-slide__heading">Добро пожаловать</p>
                        <p class="header-slide__text">Наш центр предоставляет высококвалифицированные услуги в области гинекологии женщинам всех возрастов, качество и безопасно</p>
                    </div>
                </div>
                <!-- header-slide-->
                <div class="swiper-slide header-slide">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider-img/slider-img-kt.JPG" alt="slider-img-1" class="header-slide__img">
                    <div class="container slide-content">
                        <p class="header-slide__heading">Компьютерная томография</p>
                        <p class="header-slide__text">В нашем центре открылся кабинет компьютерной томографии,как с использованием контраста, так и без, записаться можно по номеру регистратуры.</p>
                    </div>
                </div>
                <!--header-slide-->
                <div class="swiper-slide header-slide">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider-img/slider-img-operation.JPG" alt="slider-img-1" class="header-slide__img">
                    <div class="container slide-content">
                        <p class="header-slide__heading">Операции в РАГЦ</p>
                        <p class="header-slide__text">РАГЦ распологает новейшим обуродованием в области гинекологических операций, которые проводят врачи высшей категории и огромным стажем работы</p>
                    </div>
                </div>
                <!-- header-slide-->
                <div class="swiper-slide header-slide">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider-img/slider-img-palata.JPG" alt="slider-img-1" class="header-slide__img">
                    <div class="container slide-content">
                        <p class="header-slide__heading">Палаты РАГЦ</p>
                        <p class="header-slide__text">В РАГЦ доступны палаты под любой вкус и цвет, все для наших уважаемых клиентов</p>
                    </div>
                </div>
            </div>
            <!-- header-slider-pagination-->
            <div class="swiper-pagination"></div>
        </div>
        <hr class="swiper-hr">
    </div>
    <!--benefits-content-->
    <div class="container">
        <!--benefits-heading-->
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="title-h2"><?php echo esc_attr( pll__( 'Наши преимущества' ) )  ?></h2>
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/benefits-img.svg" alt="benefits-img" class="benefits-block__img">
            </div>
        </div>
        <!--benefits-content-->
        <div class="row margin--minus">
            <!--benefits-block-->
            <div class="col-12 col-md-4 col-lg-4 d-flex align-items-stretch">
                <div class="benefits-block mb-4 mb-lg-0">
                    <h3 class="title-h3 mb-3"><?php echo esc_attr( pll__( 'Квалификация врачей' ) )  ?></h3>
                    <p class="benefits-block__text"><?php echo esc_attr( pll__( 'Наши врачи систематически повышают профессиональнуюквалификацию, ведут научную деятельность и полностью отвечают мировым стандартам. В нашем коллективе работают врачи, имеющие степень Доктора PhD' ) )  ?></p>
                </div>
            </div>
            <!--benefits-block-->
            <div class="col-12 col-md-4 col-lg-4 d-flex align-items-stretch">
                <div class="benefits-block mb-4 mb-lg-0">
                    <h3 class="title-h3 mb-3"><?php echo esc_attr( pll__( 'Качество услуг' ) )  ?></h3>
                    <p class="benefits-block__text"><?php echo esc_attr( pll__( 'Профессионализм наших хирургов и акушеров повышает эффективностьлечения, обеспечивая безопасное и качественное лечение нашим пациентам, что является приоритетом в предоставлении медицинских услуг' ) )  ?></p>
                </div>
            </div>
            <!--benefits-block-->
            <div class="col-12 col-md-4 col-lg-4 d-flex align-items-stretch">
                <div class="benefits-block mb-4 mb-lg-0">
                    <h3 class="title-h3 mb-3"><?php echo esc_attr( pll__( 'Методы лечения' ) )  ?></h3>
                    <p class="benefits-block__text"><?php echo esc_attr( pll__( 'Мы используем современные методы лечения с акцентом на малоинвазивные и органосохраняющие технологии для оказания высокого уровня медицинских услуг женскому населению Карагандинской области' ) )  ?></p>
                </div>
            </div>
        </div>
    </div>
    <!--services-content-->
    <div class="container-fluid services-block__bg">
        <!--services-tittle-->
        <div class="container">
            <!--services--heading-->
            <div class="row">
                <div class="col-12">
                    <h2 class="title-h2"><?php echo esc_attr( pll__( 'Услуги' ) )  ?></h2>
                </div>
            </div>
            <!--services-container-->
            <div class="row mt-40-mobile pb-40-mobile d-flex justify-content-md-center pages-styles">
                    <?php $args = array(
                        'post_type' => 'services-category',
                        'posts_per_page' => 3,
                    );?>
                    <?php $services = new WP_Query( $args );
                    // дальше - loop
                    if( $services->have_posts() ) :
                        while( $services->have_posts() ) :
                            $services->the_post();?>
                            <!--category-block-->
                            <div class="col-12 col-md-6 col-lg-4 d-flex align-items-stretch">
                                <div class="services-category">
                                    <?php the_post_thumbnail();?>
                                    <div class="services-category__container">
                                        <h2 class="services-category__heading"><?php the_title();?></h2>
                                        <p class="services-category__text"><?php echo kama_excerpt( [ 'maxchar'=>150,  ] ); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_postdata();?>
                </div>
            <div class="row">
                <div class="col-12 text-center mb-40">
                    <a href="/services/services-category/" class="standard-link"><?php echo esc_attr( pll__( 'Все услуги' ) )  ?></a>
                </div>
            </div>
        </div>
    </div>
    <!--prices-content-->
    <div class="container">
        <!--prices-heading-->
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="title-h2">Прайс-лист</h2>
                <p class="prices-block__subtitle">Прейскурант цен на платные услуги, оказываемые в ТОО «РАГЦ» с 01
                    сентября 2021 года</p>
            </div>
        </div>
        <!--prices-block-->
        <div class="row">
            <div class="col-12 mb-40">
                <div class="prices-block">
                    <!--prices-search-->
                    <form action="#" class="prices-form">
                        <div class="prices-form__icon">
                            <input type="text" class="prices-form__search" id="pricesSearch" placeholder="Начните вводить название услуги">
                        </div>
                    </form>
                    <div class="prices-wrapper-roll">
                    <!--prices-info-->
                    <div class="prices-table prices-table--heading">
                        <p class="prices-table__heading">Наименование услуги</p>
                        <p class="prices-table__heading text-center">Стоимость (тенге)</p>
                        <p class="prices-table__heading text-center">Примечание</p>
                    </div>
                    <!--prices-wrapper-->
                    <div class="prices-wrapper prices-wrapper--bg">
                        <!--prices-category-->
                        <div class="prices-category">
                            <!--prices--heading-->
                            <p class="prices-category__heading">Консультация и диагностика:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием доцента кафедры КГМА, зам.
                                    директора по
                                    лечебной части</p>
                                <p class="prices-table__price">8 000 тг.</p>
                                <p class="prices-table__note">Один прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача акушера-гинеколога
                                </p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Один прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прием психолога</p>
                                <p class="prices-table__price">5 000 тг.</p>
                                <p class="prices-table__note">Один прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача мамолога</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Первый прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача мамолога (повторный)
                                </p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Повторный прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача онколога</p>
                                <p class="prices-table__price">5 000 тг.</p>
                                <p class="prices-table__note">Один прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультативный прием врача терапевта</p>
                                <p class="prices-table__price">5 000 тг.</p>
                                <p class="prices-table__note">Первый прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультация врача репродуктолога с УЗИ
                                    исследованием</p>
                                <p class="prices-table__price">8 000 тг.</p>
                                <p class="prices-table__note">Первый прием</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Консультация врача репродуктолога с УЗИ
                                    исследованием (повторный)</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Повторный прием</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">УЗИ диагностика:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ гинекологическое</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ фолликулометрия</p>
                                <p class="prices-table__price">2 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ лимфатических узлов </p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ мочевого пузыря</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ почек</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ плода</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ лонного сочленения</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ с доплерометрией </p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ плода с доплерометрией </p>
                                <p class="prices-table__price">4 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ гастро-дуоденальной зоны</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ мочеполовой системы комплексно </p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ молочных желез</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ щитовидной железы</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ щитовидной железы</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ предстательной железы</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ гастро-дуоденальной зоны с почками </p>
                                <p class="prices-table__price">5 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ коленных суставов </p>
                                <p class="prices-table__price">3 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ мошонки</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗИ мягких тканей и лимфоузлы</p>
                                <p class="prices-table__price">3 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ультразвуковая диагностика магистральных сосудов
                                    (БЦС)</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ультразвуковая диагностика магистральных сосудов
                                    (БЦС)</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗДГ БЦС+ транскраниальное исследование (сосудов
                                    шеи и головы)</p>
                                <p class="prices-table__price">9 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">ЭХО-КС (УЗИ сердца) </p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сосудов нижней конечности - вены
                                </p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сосудов нижней конечности - артерии
                                </p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сосудов нижней конечности - вены +
                                    артерии </p>
                                <p class="prices-table__price">11 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сосудов верхней конечности </p>
                                <p class="prices-table__price">7 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УЗДГ сосудов почек</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">НСГ</p>
                                <p class="prices-table__price">3 500 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Кольпоскопия</p>
                                <p class="prices-table__price">10 000 тг.</p>
                                <p class="prices-table__note">Одно исслед.</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Прерывание береммености:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прерывание беременности по сроку до 20-ти дней
                                    (миниаборт)</p>
                                <p class="prices-table__price">18 000 тг.</p>
                                <p class="prices-table__note">Одна проц.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прерывание беременности по сроку до 10 недель</p>
                                <p class="prices-table__price">20 000 тг.</p>
                                <p class="prices-table__note">Одна проц.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прерывание беременности в сроке от 10 до 12
                                    недель (с патологией матки)</p>
                                <p class="prices-table__price">21 000 тг.</p>
                                <p class="prices-table__note">Одна проц.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Диагностическое выскабливание полости матки в
                                    стационарных условиях</p>
                                <p class="prices-table__price">24 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    до 7-ми недель</p>
                                <p class="prices-table__price">20 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 8-10 недель</p>
                                <p class="prices-table__price">30 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 8-10 недель</p>
                                <p class="prices-table__price">30 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 10-12 недель</p>
                                <p class="prices-table__price">30 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 13 до 15-ти недель</p>
                                <p class="prices-table__price">40 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 13 до 15-ти недель</p>
                                <p class="prices-table__price">40 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Медикаментозное прерывание беременности в сроке
                                    свыше 16 до 21 недель</p>
                                <p class="prices-table__price">60 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прерывание беременности в сроке свыше 16 до 21
                                    недели путем малого «Кесарево сечения» по социальным показаниям</p>
                                <p class="prices-table__price">100 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Пребывание в палате:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Регионарная антибиотикотерапия, в сочетании с
                                    массажем и физиолечением, в условиях дневного стационара</p>
                                <p class="prices-table__price">20 000 тг.</p>
                                <p class="prices-table__note">Дневной стационар</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Лечение воспалительных процессов женской половой
                                    сферы в стационарных условиях</p>
                                <p class="prices-table__price">34 000 тг.</p>
                                <p class="prices-table__note">Стационар</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Содержание в палате с комфортными условиями - 1
                                    категории</p>
                                <p class="prices-table__price">15 000 тг.</p>
                                <p class="prices-table__note">Сутки</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Содержание в палате с комфортными условиями - 2
                                    категории</p>
                                <p class="prices-table__price">10 500 тг.</p>
                                <p class="prices-table__note">Сутки</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Дородовое наблюдение беременных</p>
                                <p class="prices-table__price">120 000 тг.</p>
                                <p class="prices-table__note">Амбулаторно</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в «Люкс-палате» после физиологических
                                    родов</p>
                                <p class="prices-table__price">158 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в «Люкс-палате» после операции
                                    «Кесарево сечение»</p>
                                <p class="prices-table__price">208 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 2-х местной палате после
                                    физиологических родов</p>
                                <p class="prices-table__price">50 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 2-х местной палате после операции
                                    «Кесарево сечение»</p>
                                <p class="prices-table__price">75 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 1 местной палате после
                                    физиологических родов</p>
                                <p class="prices-table__price">45 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 1 местной палате после операции
                                    «Кесарево сечение»</p>
                                <p class="prices-table__price">55 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 1 местной Люкс палате после
                                    физиологических родов</p>
                                <p class="prices-table__price">90 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 1 местной Люкс палате после операции
                                    «Кесарево сечение»</p>
                                <p class="prices-table__price">120 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пребывание в 2-х местной Люкс палате после
                                    физиологических родов (отдел.физ.№2)</p>
                                <p class="prices-table__price">75 000 тг.</p>
                                <p class="prices-table__note">3-5 суток</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title"> Пребывание в 2-х местной Люкс палате после
                                    операции «Кесарево сечение» (отдел.физ.№2)</p>
                                <p class="prices-table__price">100 000 тг.</p>
                                <p class="prices-table__note">5-10 суток</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Контракт с врачом:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на роды с врачом</p>
                                <p class="prices-table__price">140 000 тг.</p>
                                <p class="prices-table__note">Одна услуга</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на роды с зам. Директора, с
                                    зав.отделением</p>
                                <p class="prices-table__price">200 000 тг.</p>
                                <p class="prices-table__note">Одна услуга</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на роды с акушеркой</p>
                                <p class="prices-table__price">85 000 тг.</p>
                                <p class="prices-table__note">Одна услуга</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на кесарево сечение</p>
                                <p class="prices-table__price">160 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Контракт на кесарево сечение с зам. Директора, с
                                    зав.отделением</p>
                                <p class="prices-table__price">240 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Лапаротомные и лапароскопические операции:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пластические операции-два этапа</p>
                                <p class="prices-table__price">350 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Миома матки, бесплодие</p>
                                <p class="prices-table__price">230 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Операция на придатках</p>
                                <p class="prices-table__price">230 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Диагностическая гистероскопия </p>
                                <p class="prices-table__price">55 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Гистероскопическая полипэктомия</p>
                                <p class="prices-table__price">69 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Гистероскопическая миомэктомия </p>
                                <p class="prices-table__price">80 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Добровольная хирургическая стерилизация</p>
                                <p class="prices-table__price">150 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Экстирпация матки</p>
                                <p class="prices-table__price">240 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Вскрытие абсцесса бартолиниевой железы</p>
                                <p class="prices-table__price">40 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Киста бартолиниевой железы</p>
                                <p class="prices-table__price">75 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Пластика восстановления промежности</p>
                                <p class="prices-table__price">180 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Плазмолифтинг в гинекологии</p>
                                <p class="prices-table__price">17 500 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Эпидуральная анестезия</p>
                                <p class="prices-table__price">30 000 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Плазмоферез</p>
                                <p class="prices-table__price">30 200 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Операция подкожная мостэктомия (гинекомастия) с
                                    одной стороны</p>
                                <p class="prices-table__price">138 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Операция секторальная резекция молочной железы
                                </p>
                                <p class="prices-table__price">90 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Операция удаление добавочной молочной железы с
                                    одной стороны</p>
                                <p class="prices-table__price">100 000 тг.</p>
                                <p class="prices-table__note">Операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Иссечение доброкачественных образований</p>
                                <p class="prices-table__price">40 000 тг.</p>
                                <p class="prices-table__note">Малая операция</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Трепан-биопсия</p>
                                <p class="prices-table__price">23 500 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Трепан-биопсия с УЗИ навигацией</p>
                                <p class="prices-table__price">27 500тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Тонкоигольная аспирационная биопсия (пункционная
                                    биопсия)</p>
                                <p class="prices-table__price">10 000 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Внутривенный наркоз с пропофолом (наблюдение до
                                    полного пробуждения)</p>
                                <p class="prices-table__price">12 000 тг.</p>
                                <p class="prices-table__note">Одна процедура</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Перенатальный скрининг на преэклампсию (по
                                    Тарификатору)</p>
                                <p class="prices-table__price">11 805 тг.</p>
                                <p class="prices-table__note">Одна услуга</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Введение ВМС</p>
                                <p class="prices-table__price">12 000 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Извлечение ВМС</p>
                                <p class="prices-table__price">6 000 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Электрохирургическое лечение шейки матки</p>
                                <p class="prices-table__price">15 500 тг.</p>
                                <p class="prices-table__note">Один курс</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Лабораторные исследования:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Общий биохимический анализ крови на аппарате
                                    «Анализатор гематологический»</p>
                                <p class="prices-table__price">3 435 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение общего белка</p>
                                <p class="prices-table__price">450 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение мочевины в сыворотке крови</p>
                                <p class="prices-table__price">475 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение билирубина</p>
                                <p class="prices-table__price">535 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование крови на сахар</p>
                                <p class="prices-table__price">590 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение АЛАТ, АСАТ</p>
                                <p class="prices-table__price">445 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение креатинина</p>
                                <p class="prices-table__price">495 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Общий анализ крови на аппарате «Анализатор
                                    гематологический» </p>
                                <p class="prices-table__price">1 935 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Взятие крови</p>
                                <p class="prices-table__price">405 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение гемоглобина</p>
                                <p class="prices-table__price">355 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение эритроцитов</p>
                                <p class="prices-table__price">365 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение лейкоцитов</p>
                                <p class="prices-table__price">410 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение СОЭ </p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Развернутый анализ крови на аппарате
                                    «Анализатор гематологический»</p>
                                <p class="prices-table__price">3 290 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Взятие крови (развернутый анализ)</p>
                                <p class="prices-table__price">325 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение гемоглобина (развернутый анализ)</p>
                                <p class="prices-table__price">355 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение эритроцитов (развернутый анализ)</p>
                                <p class="prices-table__price">365 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение лейкоцитов (развернутый анализ)</p>
                                <p class="prices-table__price">410 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение СОЭ (развернутый анализ)</p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение тромбоцитов</p>
                                <p class="prices-table__price">430 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение гемотокрита</p>
                                <p class="prices-table__price">495 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование общего анализа мочи</p>
                                <p class="prices-table__price">740 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Анализ мочи по Нечипоренко</p>
                                <p class="prices-table__price">560 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Анализ мочи по Зимницкому</p>
                                <p class="prices-table__price">430 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Анализ на желчные пигменты</p>
                                <p class="prices-table__price">470 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование мазков на гонококкии и трихомоны</p>
                                <p class="prices-table__price">690 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование сахара крови глюкометром</p>
                                <p class="prices-table__price">520 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение времени свертываемости крови</p>
                                <p class="prices-table__price">410 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Кал на скрытую кровь</p>
                                <p class="prices-table__price">370 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Кал на копроскопию </p>
                                <p class="prices-table__price">700 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Молозивные тельца</p>
                                <p class="prices-table__price">365 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование околоплодных вод</p>
                                <p class="prices-table__price">370 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование мазков на антипичные клетки</p>
                                <p class="prices-table__price">490 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Коагулограмма (ПВ, ПО, ПТИ, АЧТВ, МНО, фибриноген)</p>
                                <p class="prices-table__price">1 010 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение группы крови и резус-фактора</p>
                                <p class="prices-table__price">640 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Кольпоцитология </p>
                                <p class="prices-table__price">460 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Определение антител и титра</p>
                                <p class="prices-table__price">525 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Исследование семенной жидкости </p>
                                <p class="prices-table__price">855 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Сахар мочи - тест полосками </p>
                                <p class="prices-table__price">370 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Микрореакция</p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">ВИЧ</p>
                                <p class="prices-table__price">3 800 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">ВИЧ</p>
                                <p class="prices-table__price">3 800 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Спермограмма</p>
                                <p class="prices-table__price">4 000 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">RW</p>
                                <p class="prices-table__price">3 000т тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Гепатит</p>
                                <p class="prices-table__price">4 300 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Общий бета ХГЧ на ап-те Анализатор</p>
                                <p class="prices-table__price">4 300 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Д-Димер на ап-те Анализатор </p>
                                <p class="prices-table__price">4 600 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Прокальцитон на ап-те Анализатор</p>
                                <p class="prices-table__price">7 300 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">С-реактивный белок на ап-те Анализатор</p>
                                <p class="prices-table__price">3 700 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ферритин на ап-те Анализатор</p>
                                <p class="prices-table__price">4 600 тг.</p>
                                <p class="prices-table__note">Один анализ</p>
                            </div>
                        </div>
                        <!--prices-category-->
                        <div class="prices-category">
                            <p class="prices-category__heading">Физиотерапевтические услуги:</p>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Электрофорез</p>
                                <p class="prices-table__price">530 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ректальный электрофорез</p>
                                <p class="prices-table__price">500 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УВЧ-терапия</p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Токи Бернара с введением лекарственных средств</p>
                                <p class="prices-table__price">425 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Дарсонвализация местная</p>
                                <p class="prices-table__price">440 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Микроволновая терапия (аппарат (ЛУЧ-2)</p>
                                <p class="prices-table__price">420 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УФО-местное</p>
                                <p class="prices-table__price">405 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">УФО-тубус</p>
                                <p class="prices-table__price">500 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Ультразвук</p>
                                <p class="prices-table__price">410 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Солюкс</p>
                                <p class="prices-table__price">480 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Полостной электронагреватель (ПЭН)</p>
                                <p class="prices-table__price">420 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Внутривенная инъекция</p>
                                <p class="prices-table__price">400 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Внутримышечная инъекция</p>
                                <p class="prices-table__price">355 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Система</p>
                                <p class="prices-table__price">595 тг.</p>
                                <p class="prices-table__note">Одна процед.</p>
                            </div>
                            <!--prices-table-->
                            <div class="prices-table">
                                <p class="prices-table__title">Выдача архивных справок по запросам физических лиц</p>
                                <p class="prices-table__price">1 000 тг.</p>
                                <p class="prices-table__note">Одна справка</p>
                            </div>
                        </div>
                    </div>
                    </div>
                    <button class="prices-button" onclick="changeText()">Открыть прайс-лист</button>
                </div>
            </div>
        </div>
        <!--prices-button-->
        <div class="row">
            <div class="col-12 text-center mt-4 mb-30 ">
                <a href="/services/pricelist/" class="standard-link">Весь прайс-лист</a>
            </div>
        </div>
    </div>
    <!--director-blog-content-->
    <div class="container-fluid director-block__bg">
        <div class="container">
            <div class="row">
                <!--director-blog-img-->
                <div class="col-12 col-md-5 col-lg-5 d-flex justify-content-center">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/specialists-img/director-blog-img.jpg" alt="director-blog" class="director-block__img">
                </div>
                <!--director-blog-content-->
                <div class="col-12 col-md-7 col-lg-7">
                    <div class="director-block">
                        <h2 class="title-h2"><?php echo esc_attr( pll__( 'Блог директора' ) )  ?></h2>
                        <p class="director-block__text"><?php echo esc_attr( pll__( 'Вы можете связаться с нами любым способом, который удобен для вас. Мы доступны 24/7 по факсу или электронной почте. Вы можете также использовать нашу форму связи или посетите наш Региональный Акушерско-Гинекологический Центр лично. Мы будем рады ответить на ваши вопросы' ) )  ?></p>
                        <a href="/about-us/directors-blog/" class="standard-link director-link"><?php echo esc_attr( pll__( 'Написать сообщение' ) )  ?></a>
                    </div>
                </div>
            </div>
            <!--director-blog-link-->
            <div class="row d-flex d-lg-none">
                <div class="col-12 d-flex justify-content-center mb-4">
                    <a href="/about-us/directors-blog/" class="standard-link"><?php echo esc_attr( pll__( 'Написать сообщение' ) )  ?></a>
                </div>
            </div>
        </div>
    </div>
    <!--precept-content-->
    <div class="container">
        <div class="row">
            <!--reviews-heading-->
            <div class="col-12">
                <h2 class="title-h2"><?php echo esc_attr( pll__( 'Памятка для членов семьи пациентов' ) )  ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="precept-info"><?php echo esc_attr( pll__( 'В целях профилактики острых кишечных инфекций в отделениях,
                    администрация стационара просит соблюдать требования указанные в памятке:' ) )  ?>
                </p>
            </div>
            <div class="col-12">
                <div class="precept-wrapper">
                <div class="precept-block">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/accept-icon.png" alt="" class="precept-block__img">
                    <p class="precept-block__title"><?php echo esc_attr( pll__( 'Можно передавать' ) )  ?></p>
                    <ol class="precept-list">
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Молоко в тетрапакетах - 200 мл.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Кисломолочные продукты - 500 мл.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Свежепреготовленный бульон - 500 мл.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Мясо отварное - 300 гр.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Масло сливочное - 50 гр.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Фрукты - 500 гр.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Конфеты - 250 гр.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Печенье - 200 гр.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Компот и кисель' ) )  ?> <b><?php echo esc_attr( pll__( '(домашнего приготовления)' ) )  ?></b><?php echo esc_attr( pll__( '- 500 мл.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Хлебобулочные изделия - 200 гр.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Вода без газа 1,5 литра - 2 бутылки' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Чай пакетированный - 1 упаковка' ) )  ?></li>
                    </ol>
                </div>
                <div class="precept-block">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/refuse-icon.png" alt="" class="precept-block__img">
                    <p class="precept-block__title"><?php echo esc_attr( pll__( 'Нельзя передавать' ) )  ?></p>
                    <ol class="precept-list">
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Первые и вторые блюда в объеме больше 500 мл.' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Пирожные и торты' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Колбасы и копченности' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Газированные напитки' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Шашлыки, птицы, хот-доги, фаст-фуд' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Консервы' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Салаты заправленные майонезом' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Алкогольные напитки и сигареты' ) )  ?></li>
                        <li class="precept-list__item"><?php echo esc_attr( pll__( 'Фрукты и соки' ) )  ?> <b><?php echo esc_attr( pll__( 'в послеродовое отделение' ) )  ?></b></li>
                    </ol>
                </div>
                </div>
            </div>
            <div class="col-12">
                <p class="precept-info"><b><?php echo esc_attr( pll__( 'Примечание:' ) )  ?></b>
                    <?php echo esc_attr( pll__( 'передачи для пациентов должны быть упакованы в целлофановые пакеты с указанием: ФИО пациента , отделения, номера палаты и даты передачи, а так же дожны быть осмотрены дежурным персоналом справочной.' ) )  ?>
                </p>
            </div>
        </div>
    </div>
    <!--reviews-content-->
    <div class="container-fluid reviews-slide__bg">
        <div class="container">
            <div class="row">
                <!--reviews-heading-->
                <div class="col-12">
                    <h2 class="title-h2">Отзывы</h2>
                </div>
            </div>
            <!--reviews-slider-->
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8 col-12 col-md-12">
                    <!--todo--не видно левого края-->
                    <!--reviews-container-->
                    <div class="reviews-flex">
                        <div class="swiper reviews-container reviews-slider">
                            <!--reviews-wrapper-->
                            <div class="swiper-wrapper reviews-wrapper">
                                <!--reviews-slide-->
                                <div class="swiper-slide reviews-slide">
                                    <div class="reviews-block">
                                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/reviews-icon.svg" alt="reviews-icon"
                                             class="reviews-slide__icon">
                                        <p class="reviews-slide__text">
                                            Замечательный хирург Вазенмиллер Дмитрий Владимирович. Прекрасный и отзывчивый персонал. В столовой вкусная и сытная еда. Крепкого здоровья всем сотрудникам РАГЦ
                                        </p>
                                        <p class="reviews-slide__name">Татьяна Агеева</p>
                                        <p class="reviews-slide__date">15.09.2021</p>
                                    </div>
                                </div>
                                <!--reviews-slide-->
                                <div class="swiper-slide reviews-slide">
                                    <div class="reviews-block">
                                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/reviews-icon.svg" alt="reviews-icon"
                                             class="reviews-slide__icon">
                                        <p class="reviews-slide__text">
                                            Отличный медперсонал. Рожала первый раз. Всё рассказали и очень поддержали, потому что я очень сильно боюсь докторов и всего подобного.
                                        </p>
                                        <p class="reviews-slide__name">Ксения Дубкова</p>
                                        <p class="reviews-slide__date">20.12.2021</p>
                                    </div>
                                </div>
                                <!--reviews-slide-->
                                <div class="swiper-slide reviews-slide">
                                    <div class="reviews-block">
                                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/reviews-icon.svg" alt="reviews-icon"
                                             class="reviews-slide__icon">
                                        <p class="reviews-slide__text">
                                            Рожала в мае 2020. Ситуация с пандемией была на пике, но приняли меня без проблем и без договорённости. Врачи вежливые, просто чудо санитарки и медсестры - просто прелесть, очень заботливы, чувствовала себя, как в кругу семьи. Кормят очень вкусно, убирают чисто, с ребеночком всегда помогут. В общем, спасибо большое врачам и персоналу роддома.                                        </p>
                                        <p class="reviews-slide__name">Жанар Жумагалиева</p>
                                        <p class="reviews-slide__date">26.07.2021</p>
                                    </div>
                                </div>
                                <!--reviews-slide-->
                                <div class="swiper-slide reviews-slide">
                                    <div class="reviews-block">
                                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/reviews-icon.svg" alt="reviews-icon"
                                             class="reviews-slide__icon">
                                        <p class="reviews-slide__text">
                                            Рожала в июне 2021 года. Я в восторге, потраченных денег не жалко ни копейки. Долго решалась, много читала отзывов и родила там сыночка. Питание супер. На первую ночь отдала ребёнка, чтоб прийти в себя после эпидуралки. Все, что нужно, тебе приносят. Врачи приходят 2 раза в день и отвечают на все вопросы. Спасибо рагц!                                        </p>
                                        <p class="reviews-slide__name">Василина Борисенко</p>
                                        <p class="reviews-slide__date">19.06.2021</p>
                                    </div>
                                </div>
                            </div>
                            <!--reviews-slider-pagination-->
                            <div class="swiper-pagination"></div>
                        </div>
                        <!--reviews-slider-navigation-->
                        <div class="swiper-button-next reviews-button-next"></div>
                        <div class="swiper-button-prev reviews-button-prev"></div>

                    </div>
                </div>
            </div>
            <!--specialists-button-->
            <div class="row ">
              <div class="col-12  d-flex justify-content-center mt-4" >
                   <a href="/about-us/otzyvy/" class="standard-link">Все отзывы</a>
               </div>
          </div>
        </div>
    </div>
    <!--specialists-content-->
    <div class="container-fluid specialists-block__bg">
        <div class="container">
            <div class="row">
                <!--specialists-heading-->
                <div class="col-12 ">
                    <h2 class="title-h2">Наши специалисты</h2>
                </div>
            </div>
            <!--specialists-container-->
            <div class="row mt-40">
                <div class="col-12 d-flex flex-wrap justify-content-center align-items-start">
                        <?php $args = array(
                            'post_type' => 'specialists',
                            'posts_per_page' => 6,
                        ); ?>
                        <?php $specialists = new WP_Query($args);
                        // дальше - loop
                        if ($specialists->have_posts()) :
                            while ($specialists->have_posts()) :
                                $specialists->the_post(); ?>
                                <!--specialists-block-->
                                <div class="specialists-block">
                                    <?php the_post_thumbnail(); ?>
                                    <p class="specialists-block__name"><?php the_title(); ?></p>
                                    <p class="specialists-block__position specialists-block__position--text"><?php the_field('position'); ?></p>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            <!--specialists-button-->
            <div class="row ">
                <div class="col-12  d-flex justify-content-center mb-40">
                    <a href="/about-us/specialists/" class="standard-link">Все специалисты</a>
                </div>
            </div>
        </div>
    </div>
    <!--news-content-->
    <div class="container">
    <!--news-heading-->
        <div class="row">
            <div class="col-12 ">
                <h2 class="title-h2">Новости и статьи</h2>
            </div>
        </div>
        <div class="row mt-40">
            <?php $args = array(
                'post_type' => 'post',
                'category_name'=> 'news',
                'posts_per_page' => 4
            ); ?>
            <?php $news = new WP_Query($args);
            // дальше - loop
            if ($news->have_posts()) :
                while ($news->have_posts()) :
                    $news->the_post(); ?>
                    <!--news-block-->
                    <div class="col-12 col-md-6 col-lg-12 d-flex align-items-stretch">
                        <section class="news-block">
                            <?php the_post_thumbnail( );?>
                            <article class="news-wrapper">
                                <div class="d-flex flex-column">
                                    <h3 class="title-h3 news-wrapper__heading"><?php the_title(); ?></h3>
                                    <address class="news-wrapper__author"><a href="<?php the_field('author'); ?>" rel="author" class="news-wrapper__link"><?php the_field('author'); ?></a></address>
                                    <?php echo kama_excerpt( [ 'maxchar'=>200,  ] ); ?>
                                </div>
                                <div class="news-buttons">
                                    <p class="news-buttons__date"><?php the_time( 'j F Y');?></p>
                                    <div class="d-flex align-items-center">
                                        <a href="<?php the_permalink();?>" class="header-slide__link">
                                            <span class="header-slide__span">Узнать подробнее</span>
                                            <svg class="header-slide__icon">
                                                <use xlink:href="<?php echo get_template_directory_uri() ?>/assets/img/sprite.svg#slider-arrow"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </article>
                        </section>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        </div>
 <!--news-button-->
        <div class="row mt-2">
            <div class="col-12 text-center mb-4">
                <a href="/publications/news/" class="standard-link">Все новости</a>
            </div>
        </div>
    </div>
    <!--useful-resources-->
    <div class="container">
        <!--useful-resources-heading-->
        <div class="row">
            <div class="col-12">
                <h2 class="title-h2">Полезные ресурсы</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <!--flex-container-->
                <div class="footer-flex">
                    <!--footer-slider--container-->
                    <div class="footer-container swiper footer-slider">
                        <!--footer-slider-wrapper-->
                        <div class="footer-slider__wrapper swiper-wrapper">
                            <!--footer-slider-slide-->
                            <div class="footer-slider__slide swiper-slide">
                                <a href="#" class="footer-slide-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/footer-slider-img/footer-slide-1.jpg" alt=""
                                         class="footer-slide-link__img">
                                    <p class="footer-slide-link__text">Маршрут оформления инвалидности</p>
                                </a>
                            </div>
                            <!--footer-slider-slide-->
                            <div class="footer-slider__slide swiper-slide">
                                <a href="#" class="footer-slide-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/footer-slider-img/footer-slide-2.jpg" alt=""
                                         class="footer-slide-link__img">
                                    <p class="footer-slide-link__text">Защитите будущее своего ребенка</p>
                                </a>
                            </div>
                            <!--footer-slider-slide-->
                            <div class="footer-slider__slide swiper-slide">
                                <a href="#" class="footer-slide-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/footer-slider-img/footer-slide-3.jpg" alt=""
                                         class="footer-slide-link__img">
                                    <p class="footer-slide-link__text">Министерство здравоохранения Республики Казахстан</p>
                                </a>
                            </div>
                            <!--footer-slider-slide-->
                            <div class="footer-slider__slide swiper-slide ">
                                <a href="#" class="footer-slide-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/footer-slider-img/footer-slide-4.jpg" alt=""
                                         class="footer-slide-link__img">
                                    <p class="footer-slide-link__text">Фонд социального медицинского страхования</p>
                                </a>
                            </div>
                            <!--footer-slider-slide-->
                            <div class="footer-slider__slide swiper-slide">
                                <a href="#" class="footer-slide-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/footer-slider-img/footer-slide-5.jpg" alt=""
                                         class="footer-slide-link__img">
                                    <p class="footer-slide-link__text">Вопросы и ответы по внедрению ОСМС</p>
                                </a>
                            </div>
                            <!--footer-slider-slide-->
                            <div class="footer-slider__slide swiper-slide">
                                <a href="#" class="footer-slide-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/footer-slider-img/footer-slide-5.jpg" alt=""
                                         class="footer-slide-link__img">
                                    <p class="footer-slide-link__text">Вопросы и ответы по внедрению ОСМС</p>
                                </a>
                            </div>
                            <!--footer-slider-slide-->
                            <div class="footer-slider__slide swiper-slide">
                                <a href="#" class="footer-slide-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/footer-slider-img/footer-slide-5.jpg" alt=""
                                         class="footer-slide-link__img">
                                    <p class="footer-slide-link__text">Вопросы и ответы по внедрению ОСМС</p>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                    <!--footer-slider-navigation-->
                    <div class=" footer-button-prev swiper-button-prev"></div>
                    <div class=" footer-button-next swiper-button-next"></div>

                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
