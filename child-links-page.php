<?php
/*
Template Name: Ссылки на дочерние страницы
*/
?>
<?php get_header();?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <main class="main main--margin pages-styles">
        <div class="container">
            <!--breadcrumbs-->
            <div class="row">
                <div class="col-12">
                    <?php
                    if ( function_exists( 'yoast_breadcrumb' ) ) :
                        yoast_breadcrumb( '<p class="breadcrumbs" id="breadcrumbs">', '</p>' );
                    endif;
                    ?>
                </div>
                <!--links-heading-->
                <div class="row">
                    <div class="col-12">
                        <h1 class="title-h1"><?php the_title(); ?></h1>
                    </div>
                </div>
                <!--links-content-->
                <div class="row">
                    <ul class="list-pages">
                        <?php
                        wp_list_pages( [
                        'authors'      => '',
                        'child_of'     => get_the_ID(),
                        'date_format'  => '',
                        'depth'        => 0,
                        'echo'         => 1,
                        'exclude'      => '',
                        'exclude_tree' => '',
                        'include'      => '',
                        'link_after'   => '',
                        'link_before'  => '',
                        'meta_key'     => '',
                        'meta_value'   => '',
                        'number'       => '',
                        'offset'       => '',
                        'post_type'    => 'page', // см. get_pages()
                        'show_date'    => '',
                        'sort_column'  => 'menu_order, post_title',
                        'sort_order'   => 'ASC',
                        'title_li'     => '',
                        'walker'       => '',
                        ] );
                        ?>
                    </ul>
                </div>
            </div>
    </main>
<?php endwhile; else : ?>
    <p>Записей нет.</p>
<?php endif; ?>
<?php get_footer();?>