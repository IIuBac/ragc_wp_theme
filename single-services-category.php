<?php get_header(); ?>
<!--main-content-->
<main class="main main--margin">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!--breadcrumbs-->
                <div class="row">
                    <div class="col-12">
                        <?php
                        if ( function_exists( 'yoast_breadcrumb' ) ) :
                            yoast_breadcrumb( '<p class="breadcrumbs" id="breadcrumbs">', '</p>' );
                        endif;
                        ?>
                    </div>
                </div>
                <!--heading-->
                <div class="row">
                    <div class="col-12">
                        <h1 class="title-h1"><?php the_title(); ?></h1>
                    </div>
                </div>
                <!--services-page-content-->
                <div class="row pages-styles">
                    <!--services-block-->
                    <?php get_header();?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <!--services-page-content-->
                            <div class="col-12 ">
                                <?php the_content();?>
                            </div>
                    <?php endwhile; else : ?>
                        <p>Записей нет.</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</main>
<?php get_footer(); ?>
