<!--footer-->
<footer class="footer">
    <div class="container-fluid footer-block--bg">
        <div class="container">
            <!--footer-content-->
            <div class="row">
                <!--footer-working-days-->
                <div class="col-12 col-lg-2 footer-block  order-1">
                    <p class="footer-block__heading"><?php echo esc_attr(pll__('График работы')) ?></p>
                    <p class="footer-block__subtitle mb-2"><?php echo esc_attr(pll__('Cтационар:')) ?> <span
                                class="footer-block__text">24/7</span>
                    </p>
                    <p class="footer-block__subtitle mb-2"><?php echo esc_attr(pll__('Поликлиника:')) ?></p>
                    <div class="d-flex flex-column align-items-lg-stretch">
                        <div class="footer-days">
                            <span class="footer-days__day"><?php echo esc_attr(pll__('Пн:')) ?> </span>
                            <span class="footer-days__time">8:00 — 20:00</span>
                        </div>
                        <div class="footer-days">
                            <span class="footer-days__day"><?php echo esc_attr(pll__('Вт:')) ?> </span>
                            <span class="footer-days__time">8:00 — 20:00</span>
                        </div>
                        <div class="footer-days">
                            <span class="footer-days__day"><?php echo esc_attr(pll__('Ср:')) ?> </span>
                            <span class="footer-days__time">8:00 — 20:00</span>
                        </div>
                        <div class="footer-days">
                            <span class="footer-days__day"><?php echo esc_attr(pll__('Чт:')) ?> </span>
                            <span class="footer-days__time">8:00 — 20:00</span>
                        </div>
                        <div class="footer-days">
                            <span class="footer-days__day"><?php echo esc_attr(pll__('Пт:')) ?> </span>
                            <span class="footer-days__time">8:00 — 20:00</span>
                        </div>
                        <div class="footer-days">
                            <span class="footer-days__day"><?php echo esc_attr(pll__('Сб:')) ?> </span>
                            <span class="footer-days__time">9:00 — 14:00</span>
                        </div>
                        <div class="footer-days">
                            <span class="footer-days__day"><?php echo esc_attr(pll__('Вс:')) ?></span> <span class="footer-days__time"><?php echo esc_attr(pll__('Выходной')) ?></span>
                        </div>
                    </div>
                </div>
                <!--footer-director-message-->
                <div class="col-12 col-lg-4 footer-block order-4 order-lg-2">
                    <p class="footer-block__heading"><?php echo esc_attr(pll__('Послание Директора')) ?></p>
                    <p class="footer-block__text">
                        <?php echo esc_attr(pll__('Наша миссия - это улучшение здоровья нации путем охраны здоровья матери и ребенка, профилактики и своевременного лечения гинекологических заболеваний на основе доказательной медицины и доверия пациентов, используя современные достижения медицинской науки и техники, высокую квалификацию медицинских работников')) ?>
                    </p>
                    <p class="footer-block__subtitle mt-2">Юзик.Т.А.</p>
                </div>
                <!---footer-social-links-->
                <div class="col-12 col-lg-3 footer-block order-2 order-lg-3">
                    <p class="footer-block__heading"><?php echo esc_attr(pll__('Контакты')) ?></p>
                    <div class="d-flex flex-column align-items-lg-start">
                        <a href="tel:+77078837530" class="info-block__link info-block__link--footer"><span
                                    class="info-block__text"><?php echo esc_attr(pll__('Регистратура:')) ?></span>
                            +7
                            707 88 375 30</a>
                        <a href="tel:+77078837533" class="info-block__link info-block__link--footer"><span
                                    class="info-block__text"><?php echo esc_attr(pll__('Акушерский стационар:')) ?></span> +7 707 88 375 33</a>
                        <a href="tel:+77078837530" class="info-block__link info-block__link--footer"><span
                                    class="info-block__text"><?php echo esc_attr(pll__('Гинеколог.стационар:')) ?></span> +7 707 88 375 31</a>
                        <a href="mailto:ragc@mail.ru" class="info-block__link info-block__link--footer"><span
                                    class="info-block__ text"><?php echo esc_attr(pll__('Почта:')) ?> </span>ragc@mail.ru</a>
                        <a href="https://www.instagram.com/ragc_krg2017/?hl=ru"
                           class="info-block__link info-block__link--footer" target="_blank"><span
                                    class="info-block__ text">Instagram:</span>
                            ragc_krg2017</a>
                        <a href="https://2gis.kz/karaganda/firm/11822477302868662?m=73.200061%2C49.881088%2F18.98"
                           class="info-block__link info-block__link--footer" target="_blank"><span
                                    class="info-block__ text"><?php echo esc_attr(pll__('Адрес:')) ?> </span>
                            <?php echo esc_attr(pll__('Караганда, Казахстан, ул. Щорса, 53')) ?></a>
                    </div>
                </div>
                <!--footer-usefull-links-->
                <div class="col-12 col-lg-3 footer-block order-3 order-lg-4">
                    <p class="footer-block__heading"><?php echo esc_attr(pll__('Полезная информация')) ?></p>
                    <div class="d-flex flex-column">
                        <a href="#" class="footer-block__link"><?php echo esc_attr(pll__('Для беременных')) ?></a>
                        <a href="#" class="footer-block__link"><?php echo esc_attr(pll__('ОСМС')) ?></a>
                        <a href="#" class="footer-block__link"><?php echo esc_attr(pll__(' Антикоррупция')) ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
