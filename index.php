<?php get_header();?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <main class="main main--margin pages-styles">
        <div class="container">
            <!--breadcrumbs-->
            <div class="row">
                <div class="col-12">
                    <?php
                        if ( function_exists( 'yoast_breadcrumb' ) ) :
                            yoast_breadcrumb( '<p class="breadcrumbs" id="breadcrumbs">', '</p>' );
                        endif;
                        ?>
            </div>
            <!--contacts-heading-->
            <div class="row">
                <div class="col-12">
                    <h1 class="title-h1"><?php the_title(); ?></h1>
                </div>
            </div>
            <!--contacts-content-->
            <div class="row">
                    <?php the_content(); ?>
            </div>
        </div>
    </main>
<?php endwhile; else : ?>
    <p>Записей нет индекс.</p>
<?php endif; ?>
<?php get_footer();?>
