<?php
//Translate//
add_action('init', 'ragc_polylang_strings' );

function ragc_polylang_strings() {

    if( ! function_exists( 'pll_register_string' ) ) {
        return;
    }
//header//
    pll_register_string(
        'Registry',
        'Регистратура:',
        'Header',
        false

    );
    pll_register_string(
        'Obstetric-hospital',
        'Акушерский стационар:',
        'Header',
        false

    );
    pll_register_string(
        'Gynecological-hospital',
        'Гинекологический стационар:',
        'Header',
        false

    );
    pll_register_string(
        'Mail',
        'Почта:',
        'Header',
        false

    );
    pll_register_string(
        'Blind-version',
        'Версия для слабовидящих',
        'Header',
        false

    );
    pll_register_string(
        'RAGC',
        'Региональный Акушерско Гинекологический Центр',
        'Header',
        false

    );
    pll_register_string(
        'Navbar',
        'Меню',
        'Header',
        false

    );
//footer//
    pll_register_string(
        'Schedule',
        'График работы',
        'Footer',
        false

    );
    pll_register_string(
        'Stationary',
        'Cтационар:',
        'Footer',
        false

    );
    pll_register_string(
        'Polyclinic',
        'Поликлиника:',
        'Footer',
        false

    );
    pll_register_string(
        'Monday',
        'Пн:',
        'Footer',
        false

    );
    pll_register_string(
        'Tuesday',
        'Вт:',
        'Footer',
        false

    );
    pll_register_string(
        'Wednesday',
        'Ср:',
        'Footer',
        false

    );
    pll_register_string(
        'Thursday',
        'Чт:',
        'Footer',
        false

    );
    pll_register_string(
        'Friday',
        'Пт:',
        'Footer',
        false

    );
    pll_register_string(
        'Saturday',
        'Сб:',
        'Footer',
        false

    );
    pll_register_string(
        'Sunday',
        'Вс:',
        'Footer',
        false

    );

    pll_register_string(
        'Day-off',
        'Выходной:',
        'Footer',
        false

    );
    pll_register_string(
        'Directors-Message',
        'Послание Директора',
        'Footer',
        false

    );
    pll_register_string(
        'Message',
        'Наша миссия - это улучшение здоровья нации путем охраны здоровья матери и ребенка, профилактики и своевременного лечения гинекологических заболеваний на основе доказательной медицины и доверия пациентов, используя современные достижения медицинской науки и техники, высокую квалификацию медицинских работников',
        'Footer',
        false

    );
    pll_register_string(
        'Contacts',
        'Контакты',
        'Footer',
        false

    );
    pll_register_string(
        'Address',
        'Адрес:',
        'Footer',
        false

    );
    pll_register_string(
        'Karaganda',
        'Караганда, Казахстан, ул. Щорса, 53',
        'Footer',
        false

    );
    pll_register_string(
        'Useful-information',
        'Полезная информация',
        'Footer',
        false

    );
    //benefits-content/
    pll_register_string(
        'Our-advantages',
        'Наши преимущества',
        'benefits-content',
        false

    );
    pll_register_string(
        'Doctors-qualification',
        'Квалификация врачей',
        'benefits-content',
        false

    );
    pll_register_string(
        'Doctors-qualification-text',
        'Наши врачи систематически повышают профессиональнуюквалификацию, ведут научную деятельность и полностью отвечают мировым стандартам. В нашем коллективе работают врачи, имеющие степень Доктора PhD',
        'benefits-content',
        false

    );
    pll_register_string(
        'Service-quality',
        'Качество услуг',
        'benefits-content',
        false

    );
    pll_register_string(
        'Service-quality-text',
        'Профессионализм наших хирургов и акушеров повышает эффективностьлечения, обеспечивая безопасное и качественное лечение нашим пациентам, что является приоритетом в предоставлении медицинских услуг',
        'benefits-content',
        false

    );
    pll_register_string(
        'Treatment-methods',
        'Методы лечения',
        'benefits-content',
        false

    );
    pll_register_string(
        'Treatment-methods-text',
        'Мы используем современные методы лечения с акцентом на малоинвазивные и органосохраняющие технологии для оказания высокого уровня медицинских услуг женскому населению Карагандинской области',
        'benefits-content',
        false

    );
    //services-content/
    pll_register_string(
        'Services',
        'Услуги',
        'services-content',
        false

    );
    pll_register_string(
        'Services-all',
        'Все Услуги',
        'services-content',
        true

    );
    pll_register_string(
        'More',
        'Подробнее',
        'services-content',
        true

    );
    //specialists-content/
    pll_register_string(
        'Work-experience',
        'Места и стаж работы:',
        'specialists-content',
        true

    );
    pll_register_string(
        'Education',
        'Образование:',
        'specialists-content',
        true

    );
    pll_register_string(
        'Speciality',
        'Специальность:',
        'specialists-content',
        true

    );
    pll_register_string(
        'Position',
        'Должность:',
        'specialists-content',
        true

    );
    pll_register_string(
        'Position',
        'Курсы повышения квалификации:',
        'specialists-content',
        true

    );
    pll_register_string(
        'Category',
        'Квалификационная категория:',
        'specialists-content',
        true

    );
    pll_register_string(
        'Awards',
        'Награды:',
        'specialists-content',
        true

    );
    pll_register_string(
        'Back',
        'Назад',
        'specialists-content',
        true

    );
    //pricelist-content/
    pll_register_string(
        'Services',
        'Услуги',
        'specialists-content',
        false

    );
    pll_register_string(
        'Services-all',
        'Все Услуги',
        'specialists-content',
        true

    );
    //director-blog-content/
    pll_register_string(
        'Director-blog',
        'Блог директора',
        'director-blog-content',
        false

    );
    pll_register_string(
        'Director-message',
        'Вы можете связаться с нами любым способом, который удобен для вас. Мы доступны 24/7 по факсу или электронной почте. Вы можете также использовать нашу форму связи или посетите наш Региональный Акушерско-Гинекологический Центр лично. Мы будем рады ответить на ваши вопросы',
        'director-blog-content',
        true

    );
    pll_register_string(
        'Add-message',
        'Написать сообщение',
        'director-blog-content',
        true

    );
    //precept-content/
    pll_register_string(
        'Memo',
        'Памятка для членов семьи пациентов',
        'precept-content',
        false

    );
    pll_register_string(
        'Memo-message',
        'В целях профилактики острых кишечных инфекций в отделениях, администрация стационара просит соблюдать требования указанные в памятке:',
        'precept-content',
        true

    );
    pll_register_string(
        'Accept',
        'Можно передавать',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-1',
        'Молоко в тетрапакетах - 200 мл.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-2',
        'Кисломолочные продукты - 500 мл.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-3',
        'Свежепреготовленный бульон - 500 мл.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-4',
        'Мясо отварное - 300 гр.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-5',
        'Масло сливочное - 50 гр.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-6',
        'Фрукты - 500 гр.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-7',
        'Конфеты - 250 гр.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-8',
        'Печенье - 200 гр.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-9',
        'Компот и кисель',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-9.1',
        '(домашнего приготовления)',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-9.2',
        '- 500 мл.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-10',
        'Хлебобулочные изделия - 200 гр.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-11',
        'Хлебобулочные изделия - 200 гр.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-12',
        'Хлебобулочные изделия - 200 гр.',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-13',
        'Вода без газа 1,5 литра - 2 бутылки',
        'precept-content',
        false

    );
    pll_register_string(
        'Accept-14',
        'Чай пакетированный - 1 упаковка',
        'precept-content',
        false

    );
}

//error 500//
if( ! function_exists( 'pll__' ) ) {
    function pll__( $string ) {
        return $string;
    }
}

if( ! function_exists( 'pll_e' ) ) {
    function pll_e( $string ) {
        echo $string;
    }
}
